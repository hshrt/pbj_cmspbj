<?php 

//this script all william keys to the DB
require("../config.php");
require("../php/inc.appvars.php");
require("../php/func_nx.php");

session_start();

/*$williamobj = json_decode('[{"lang":"ru","data":"The Verandah"},
{"lang":"zh-ct","data":"吉地士"},
{"lang":"zh-cs","data":"吉地士"},
{"lang":"ja","data":"ガディス"},
{"lang":"fr-fr","data":"Gaddi’s"},
{"lang":"en-us","data":"Gaddi's"},
{"lang":"ar","data":"غاديز"},
{"lang":"de","data":"Gaddi's"},
{"lang":"ko","data":"가디스(Gaddi's)"},
{"lang":"es","data":"Gaddi's"},
{"lang":"ra","data":"Gaddi's"},
{"lang":"pt","data":"Gaddi's"}]}'
,true);*/

$dictArr = array(
    array("key"=>"mm.bluray","English"=>"Bluray","Arabic"=>"مُشغل بلو راي","German"=>"Blu-Ray","French"=>"Bluray","Spanish"=>"Blu-ray","Japanese"=>"ブルーレイ","Korean"=>"블루레이","Portuguese"=>"Blu-ray","Russian"=>"Bluray","Simplified Chinese"=>"蓝光","Traditional Chinese"=>"藍光"),
    array("key"=>"mm.smartPhoneDock","English"=>"Smart Phone Dock","Arabic"=>"قاعدة شحن الهواتف الذكية","German"=>"Smartphone-Dockingstation","French"=>"Station pour smartphone","Spanish"=>"Puerto para teléfono inteligente","Japanese"=>"スマートフォンドック","Korean"=>"스마트폰 독","Portuguese"=>"Base para smartphone","Russian"=>"Док-станция для смартфона","Simplified Chinese"=>"智能手机插座","Traditional Chinese"=>"智能手機底​座"),
    array("key"=>"mm.externalInput","English"=>"External Input","Arabic"=>"وحدات إدخال خارجية","German"=>"Externer Eingang","French"=>"Prise externe","Spanish"=>"Entrada externa","Japanese"=>"外部入力","Korean"=>"외부 입력","Portuguese"=>"Entrada externa","Russian"=>"Внешний ввод","Simplified Chinese"=>"外置输入","Traditional Chinese"=>"外置輸入"),
    array("key"=>"mm.cardReader","English"=>"Card Reader","Arabic"=>"قارئ البطاقات","German"=>"Kartenleser","French"=>"Lecteur de carte","Spanish"=>"Lector de tarjetas","Japanese"=>"カードリーダー","Korean"=>"카드 리더","Portuguese"=>"Leitor de cartão","Russian"=>"Устройство считывания карт памяти","Simplified Chinese"=>"读卡器","Traditional Chinese"=>"讀卡器 "),
    array("key"=>"Multimedia","English"=>"Multimedia","Arabic"=>"وسائط متعددة","German"=>"Multimedia","French"=>"Multimédia","Spanish"=>"Multimedios","Japanese"=>"マルチメディア","Korean"=>"멀티미디어","Portuguese"=>"Multimídia","Russian"=>"Мультимедийная система","Simplified Chinese"=>"多媒体","Traditional Chinese"=>"多媒體"),
    array("key"=>"mm.menu","English"=>"menu","Arabic"=>"القائمة","German"=>"Menü","French"=>"menu","Spanish"=>"menú","Japanese"=>"メニュー","Korean"=>"메뉴","Portuguese"=>"menu","Russian"=>"меню","Simplified Chinese"=>"菜单","Traditional Chinese"=>"主目錄"),
    array("key"=>"mm.back","English"=>"back","Arabic"=>"رجوع","German"=>"Zurück","French"=>"retour","Spanish"=>"atrás","Japanese"=>"戻る","Korean"=>"뒤","Portuguese"=>"voltar","Russian"=>"назад","Simplified Chinese"=>"返回","Traditional Chinese"=>"返回"),
    array("key"=>"OK","English"=>"OK","Arabic"=>"موافق","German"=>"OK","French"=>"OK","Spanish"=>"Aceptar","Japanese"=>"OK","Korean"=>"확인","Portuguese"=>"OK","Russian"=>"OK","Simplified Chinese"=>"确定","Traditional Chinese"=>"確定"),
    array("key"=>"dining","English"=>"Dining","Arabic"=>"تناول الطعام","German"=>"Speisen","French"=>"Restauration","Spanish"=>"Gastronomía","Japanese"=>"ダイニング","Korean"=>"다이닝","Portuguese"=>"Restaurantes","Russian"=>"Рестораны","Simplified Chinese"=>"餐饮","Traditional Chinese"=>"餐飲"),
    array("key"=>"No messages","English"=>"No messages","Arabic"=>"لا توجد رسائل","German"=>"Keine Mitteilungen","French"=>"Aucun message","Spanish"=>"Sin mensajes","Japanese"=>"メッセージなし","Korean"=>"메시지 없음","Portuguese"=>"Nenhuma mensagem","Russian"=>"Нет сообщений","Simplified Chinese"=>"没有信息","Traditional Chinese"=>"沒有訊息"),
    array("key"=>"Watch","English"=>"Watch","Arabic"=>"مشاهدة","German"=>"Ansehen","French"=>"Regarder","Spanish"=>"Reproducir","Japanese"=>"時計","Korean"=>"보기","Portuguese"=>"Assistir","Russian"=>"Смотреть","Simplified Chinese"=>"观看","Traditional Chinese"=>"觀看"),
    array("key"=>"Call","English"=>"Call","Arabic"=>"اتصال","German"=>"Anrufen","French"=>"Appel","Spanish"=>"Llamar","Japanese"=>"呼び出し","Korean"=>"통화","Portuguese"=>"Chamar","Russian"=>"Вызов","Simplified Chinese"=>"呼叫","Traditional Chinese"=>"召喚"),
    array("key"=>"Review order","English"=>"Review order","Arabic"=>"مراجعة الطلب","German"=>"Bestellung prüfen","French"=>"Vérifier la commande","Spanish"=>"Revisar pedido","Japanese"=>"注文の確認","Korean"=>"주문 검토","Portuguese"=>"Revisar pedido","Russian"=>"Просмотреть заказ","Simplified Chinese"=>"查阅点单","Traditional Chinese"=>"查閱已點食物"),
    array("key"=>"Edit","English"=>"Edit","Arabic"=>"تعديل","German"=>"Bearbeiten","French"=>"Modifier","Spanish"=>"Editar","Japanese"=>"編集","Korean"=>"편집","Portuguese"=>"Editar","Russian"=>"Редактировать","Simplified Chinese"=>"更改点单","Traditional Chinese"=>"更改"),
    array("key"=>"Order Time","English"=>"Order Time","Arabic"=>"وقت تقديم الطلب","German"=>"Uhrzeit","French"=>"Heure souhaitée du service","Spanish"=>"Horario del pedido","Japanese"=>"注文指定時間","Korean"=>"주문 시간","Portuguese"=>"Horário do pedido","Russian"=>"Время заказа","Simplified Chinese"=>"送餐时间","Traditional Chinese"=>"送餐時間"),
    array("key"=>"Submit","English"=>"Submit","Arabic"=>"أرسل","German"=>"Absenden","French"=>"Envoyer","Spanish"=>"Enviar","Japanese"=>"送信","Korean"=>"제출","Portuguese"=>"Enviar","Russian"=>"Отправить","Simplified Chinese"=>"提交","Traditional Chinese"=>"提交"),
    array("key"=>"INROOM_WILL_SERVE","English"=>"This order will serve","Arabic"=>"سيُقدم هذا الطلب","German"=>"Diese Bestellung enthält","French"=>"Cette commande servira","Spanish"=>"Este pedido se servirá","Japanese"=>"ご注文の品の提供時間","Korean"=>"이 주문은 서비스될 것입니다","Portuguese"=>"Este pedido será atendido","Russian"=>"Вы заказали","Simplified Chinese"=>"此点单将供应","Traditional Chinese"=>"已點食物將供應"),
    array("key"=>"INROOM_REVIEW_ORDER","English"=>"Please review your order","Arabic"=>"يُرجى التكرم بمراجعة طلبك","German"=>"Bitte prüfen Sie Ihre Bestellung","French"=>"Veuillez vérifier votre commande","Spanish"=>"Sírvase revisar su pedido","Japanese"=>"ご注文の品をご確認ください","Korean"=>"주문을 검토하시기 바랍니다","Portuguese"=>"Revise seu pedido","Russian"=>"Проверьте свой заказ","Simplified Chinese"=>"请查阅您的点单","Traditional Chinese"=>"請查閱閣下的食物訂單"),
    array("key"=>"checkout_intro","English"=>"Please be advised that by using the Priority Check-Out, we will check-out your room in 15 minutes. The credit card you provided upon arrival will be billed for your charges and expenses during the stay.","Arabic"=>"يُرجى العلم أن باستخدامكم خدمة أسبقية تسجيل المغادرة، ستنتهي إجراءات تسجيل المغادرة في غضون 15 دقيقة. سيجرى احتساب النفقات والرسوم خلال مدة إقامتكم، وخصمها من بطاقة الائتمان التي قُدِمت من جانبكم عند الوصول.","German"=>"Bitte beachten Sie, dass wir Ihr Zimmer in 15 Minuten auschecken, wenn Sie den Priority Check-Out in Anspruch nehmen. Die bei Ihrer Ankunft vorgelegte Kreditkarte wird mit Ihren Gebühren und Ausgaben während Ihres Aufenthalts belastet.","French"=>"Notez que si vous utilisez le départ prioritaire, vous partirez de votre chambre dans 15 minutes. La carte de crédit indiquée à l’arrivée sera débitée des frais et dépenses de votre séjour.","Spanish"=>"Tenga en cuenta que al utilizar el Check-Out prioritario, haremos el check-out de su habitación en 15 minutos. Cobraremos a la tarjeta de crédito que proporcionó al momento de su llegada todos sus cargos y gastos en que incurrió durante su estadía.","Japanese"=>"優先チェックアウトをご利用の場合は、15分以内にチェックアウトのお手続きを完了します。ご到着時にご提示いただいたクレジットカードにご滞在中のご利用代金を課金いたします。","Korean"=>"우선 체크아웃을 사용하면, 객실이 15분 뒤에 체크아웃되므로 참고하시기 바랍니다. 도착시 제공하신 신용카드로 숙박하신 동안 사용하신 수수료 및 비용이 청구됩니다. ","Portuguese"=>"Informamos que, ao usar o check-out prioritário, o seu check-out é realizado dentro de 15 minutos. Os gastos contraídos durante a estada serão cobrados no cartão de crédito fornecido na chegada.","Russian"=>"Обращаем ваше внимание на то, что при использовании опции приоритетной регистрации отъезда вы будете выписаны из номера через 15 минут. Сумма всех расходов за время вашего пребывания в отеле будет списана с кредитной карты, информацию о которой вы указали по прибытии.","Simplified Chinese"=>"请注意优先退房服务需时 15 分钟完成。您住宿期间的费用和开支将通过登记入住时提供的信用卡入账。","Traditional Chinese"=>"請注意優先退房服務需時 15 分鐘處理。閣下住宿期間的費用及開支將從登記入住時提供的信用卡扣賬。"),
    array("key"=>"check_out_item1","English"=>"Request an Invoice","Arabic"=>"طلب فاتورة","German"=>"Eine Rechnung anfordern","French"=>"Demander une facture","Spanish"=>"Solicitar una factura","Japanese"=>"請求書をリクエストする","Korean"=>"계산서 요청","Portuguese"=>"Solicitar fatura","Russian"=>"Запросить счет","Simplified Chinese"=>"索取发票","Traditional Chinese"=>"要求發票"),
    array("key"=>"check_out_item2","English"=>"Request Baggage Assistance","Arabic"=>"طلب مساعدة في حمل الأمتعة","German"=>"Hilfe beim Gepäck anfordern","French"=>"Demander de l’aide pour les bagages","Spanish"=>"Solicitar asistencia con el equipaje","Japanese"=>"手荷物の移動をリクエストする","Korean"=>"수하물 지원 요청","Portuguese"=>"Solicitar retirada de bagagens","Russian"=>"Запросить помощь с багажом","Simplified Chinese"=>"要求行李服务","Traditional Chinese"=>"要求行李服務"),
    array("key"=>"check_out_item3","English"=>"No Invoice is required","Arabic"=>"لا أحتاج إلى فاتورة","German"=>"Keine Rechnung erforderlich","French"=>"Aucune facture nécessaire","Spanish"=>"No se necesita factura","Japanese"=>"請求書は不要です","Korean"=>"계산서가 필요하지 않음","Portuguese"=>"Fatura não necessária","Russian"=>"Счет не требуется","Simplified Chinese"=>"无需发票","Traditional Chinese"=>"無需發票"),
    array("key"=>"CHECKOUT","English"=>"Check-Out","Arabic"=>"تسجيل المغادرة","German"=>"Check-out","French"=>"Départ","Spanish"=>"Check-Out","Japanese"=>"チェックアウト","Korean"=>"체크아웃","Portuguese"=>"Check-out","Russian"=>"Регистрация отъезда","Simplified Chinese"=>"退房","Traditional Chinese"=>"退房"),
    array("key"=>"CHECKOUT_THANKS","English"=>"Thank you for using the Priority Check-Out.","Arabic"=>"شكرًا لاستخدامكم خدمة أسبقية تسجيل المغادرة.","German"=>"Vielen Dank, dass Sie den Priority Check-Out nutzen.","French"=>"Merci d’avoir utilisé le départ prioritaire.","Spanish"=>"Gracias por usar el Check-Out prioritario.","Japanese"=>"優先チェックアウトをご利用いただきありがとうございます。","Korean"=>"우선 체크아웃을 사용하여 주셔서 감사합니다. ","Portuguese"=>"Agradecemos por usar o check-out prioritário.","Russian"=>"Спасибо за использование опции приоритетной регистрации отъезда.","Simplified Chinese"=>"感谢使用优先退房服务。","Traditional Chinese"=>"多謝使用優先退房服務。"),
    array("key"=>"CHECKOUT_INVOICE","English"=>"Please collect your invoice at the Cashier Desk upon departure.","Arabic"=>"يُرجى التكرم باستلام فاتورتكم من مكتب الحسابات عند المغادرة.","German"=>"Bitte holen Sie Ihre Rechnung bei der Abreise am Kassenschalter ab.","French"=>"Veuillez récupérer votre facture à l’accueil en partant.","Spanish"=>"Al momento de partir, sírvase recoger su factura en el escritorio del cajero.","Japanese"=>"ご出発の際に、ご精算デスクにてご請求書をお受け取りください。","Korean"=>"출발시 캐셔 데스크에서 계산서를 받아가십시오. ","Portuguese"=>"Pegue a fatura no balcão da recepção na partida.","Russian"=>"Пожалуйста, получите счет на стойке регистрации перед отъездом из отеля.","Simplified Chinese"=>"离开前请到收银处领取发票。","Traditional Chinese"=>"離開前請到收銀處領取發票。"),
    array("key"=>"CHECKOUT_END","English"=>"We look forward to welcoming you back to ###.","Arabic"=>"سنشتاق كثيرًا إليكم، على أمل أن نراكم بيننا قريبًا في ###.","German"=>"Wir freuen uns darauf, Sie bald wieder im ### begrüßen zu dürfen.","French"=>"Nous espérons vous accueillir à nouveau à ###.","Spanish"=>"Esperamos recibirlo nuevamente en ###.","Japanese"=>"###へのまたのお越しをお待ちしております。","Korean"=>"###에 다시 방문해 주시기를 기대합니다. ","Portuguese"=>"Esperamos receber você novamente em ###.","Russian"=>"Ждем вас снова в ###.","Simplified Chinese"=>"我们期待再次在 ### 为您服务。","Traditional Chinese"=>"我們期待再次在 ### 為閣下服務。"),
    array("key"=>"CHECKOUT_BAGGAGE","English"=>"We will come to collect your baggage in approximately 10 minutes.","Arabic"=>"سيأتي الموظف لاستلام أمتعتكم في غضون 10 دقائق تقريبًا.","German"=>"Wir werden Ihr Gepäck in etwa zehn Minuten abholen.","French"=>"Nous viendrons chercher vos bagages dans une dizaine de minutes.","Spanish"=>"Recogeremos su equipaje en aproximadamente 10 minutos.","Japanese"=>"約10分ほどで、お手荷物をお預かりする担当者がまいります。","Korean"=>"약 10분 뒤에 방문하여 수하물을 수집하겠습니다. ","Portuguese"=>"Retiraremos sua bagagem em aproximadamente 10 minutos.","Russian"=>"Мы заберем ваш багаж приблизительно через 10 минут.","Simplified Chinese"=>"我们将在大约 10 分钟内来收集您的行李。","Traditional Chinese"=>"我們將約在 10 分鐘後前來領取閣下的行李。"),
    array("key"=>"Loading","English"=>"Loading...","Arabic"=>"تحميل...","German"=>"Wird geladen…","French"=>"Téléchargement en cours...","Spanish"=>"Cargando...","Japanese"=>"ロード中... ","Korean"=>"로드 중... ","Portuguese"=>"Carregando...","Russian"=>"Загружается...","Simplified Chinese"=>"载入中。。。。。。","Traditional Chinese"=>"載入中…"),
    array("key"=>"flight.active","English"=>"Active","Arabic"=>"رحلة نشطة","German"=>"Aktiv","French"=>"Actif","Spanish"=>"Activo","Japanese"=>"アクティブ","Korean"=>"활성화","Portuguese"=>"Ativo","Russian"=>"Активен","Simplified Chinese"=>"使用中 ","Traditional Chinese"=>"使用中 "),
    array("key"=>"flight.canceled","English"=>"Canceled","Arabic"=>"الرحلة أُلغيت","German"=>"Annulliert","French"=>"Annulé","Spanish"=>"Cancelado","Japanese"=>"キャンセルされました","Korean"=>"취소됨","Portuguese"=>"Cancelado","Russian"=>"Отменен","Simplified Chinese"=>"已取消","Traditional Chinese"=>"已取消"),
    array("key"=>"flight.diverted","English"=>"Diverted","Arabic"=>"الرحلة هبطت إلى مطار آخر","German"=>"Umgeleitet","French"=>"Dérouté","Spanish"=>"Desviado","Japanese"=>"変更されました","Korean"=>"전환됨","Portuguese"=>"Transferido","Russian"=>"Приземлился в другом аэропорту","Simplified Chinese"=>"已改道","Traditional Chinese"=>"已改道"),
    array("key"=>"flight.data_source_needed","English"=>"Data source needed","Arabic"=>"مصدر البيانات مطلوب","German"=>"Datenquelle erforderlich","French"=>"Source de données nécessaire","Spanish"=>"Se necesita el origen de los datos","Japanese"=>"データソースが必要です","Korean"=>"데이터 소스 필요함","Portuguese"=>"Fonte de dados necessária","Russian"=>"Нет источника данных","Simplified Chinese"=>"需要数据源","Traditional Chinese"=>"需要資料來源"),
    array("key"=>"flight.landed","English"=>"Landed","Arabic"=>"الرحلة هبطت إلى المطار المحدد","German"=>"Gelandet","French"=>"Atterri","Spanish"=>"Aterrizado","Japanese"=>"着陸しました","Korean"=>"도착함","Portuguese"=>"Aterrissado","Russian"=>"Приземлился","Simplified Chinese"=>"已着陆","Traditional Chinese"=>"已著陸"),
    array("key"=>"flight.not_operational","English"=>"Not Operational","Arabic"=>"الرحلة غير مجدوّلة زمنيًا","German"=>"Nicht in Betrieb","French"=>"Non opérationnel","Spanish"=>"No funciona","Japanese"=>"ご利用いただけません","Korean"=>"작동하지 않음","Portuguese"=>"Não operacional","Russian"=>"Не обслуживается","Simplified Chinese"=>"不能操作","Traditional Chinese"=>"不能操作  "),
    array("key"=>"flight.redirected","English"=>"Redirected","Arabic"=>"الرحلة غيّرت مسارها","German"=>"Umgeleitet","French"=>"Redirigé","Spanish"=>"Redirigido","Japanese"=>"転送されました","Korean"=>"리다이렉트됨","Portuguese"=>"Redirecionado","Russian"=>"Перенаправлен","Simplified Chinese"=>"已重新导向","Traditional Chinese"=>"已重新導向"),
    array("key"=>"flight.scheduled","English"=>"Scheduled","Arabic"=>"الرحلة مجدوّلة زمنيًا","German"=>"Geplant","French"=>"Programmé","Spanish"=>"Programado","Japanese"=>"手配されました","Korean"=>"예약됨","Portuguese"=>"Programado","Russian"=>"По расписанию","Simplified Chinese"=>"已安排","Traditional Chinese"=>"已安排 "),
    array("key"=>"flight.unknown","English"=>"Unknown","Arabic"=>"غير معروف","German"=>"Unbekannt","French"=>"Inconnu","Spanish"=>"Desconocido","Japanese"=>"不明","Korean"=>"알 수 없음","Portuguese"=>"Desconhecido","Russian"=>"Неизвестно","Simplified Chinese"=>"不明","Traditional Chinese"=>"不明")
);


//pprint_r($dictArr);



/*pprint_r($obj);

pprint_r(array_keys($obj,true));*/



$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");
echo "fuck";

foreach($dictArr as $v) {

    pprint_r($v);
        $ru = $v["Russian"];


        $zhct = $v["Traditional Chinese"];

        $zhcs = $v["Simplified Chinese"];


        $ja = $v["Japanese"];


        $fr = $v["French"];


        $en = $v["English"];


        $ar = $v["Arabic"];


        $de = $v["German"];


        $ko = $v["Korean"];


        $es = $v["Spanish"];



        $pt = $v["Portuguese"];

        $key = $v["key"];


pprint_r($key);
pprint_r($ru);
pprint_r($zhct);
pprint_r($zhcs);
pprint_r($ja);
pprint_r($fr);
pprint_r($en);
pprint_r($ar);
pprint_r($de);
pprint_r($ko);
pprint_r($es);
pprint_r($pt);


    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ){
        $list[] = $row;
    }

    $uuid = $list[0]["UUID"];

    /*$sql = "INSERT INTO dictionary (id,en) VAL UES (:id,:title)";
    $st = $conn->prepare ( $sql );

    $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
    $st->bindValue( ":title", $title, PDO::PARAM_INT );
    $st->execute();
    $titleId = $uuid;*/

    $sql = "INSERT INTO dictionary (dictionary.id,dictionary.key, en, zh_hk, zh_cn,jp, fr, ar, es, de, ko, ru,
    pt,lastUpdate, lastUpdateBy ) VALUES (UUID(),:titleKey,
    :en, :zh_hk, :zh_cn, :jp, :fr, :ar, :es, :de,:ko, :ru, :pt,now(),:email )";

    $st = $conn->prepare ( $sql );
    $st->bindValue( ":titleKey", $key, PDO::PARAM_STR);
    $st->bindValue( ":en", $en, PDO::PARAM_STR);
    $st->bindValue( ":zh_hk", $zhct, PDO::PARAM_STR);
    $st->bindValue( ":zh_cn", $zhcs, PDO::PARAM_STR);
    $st->bindValue( ":jp", $ja, PDO::PARAM_STR);
    $st->bindValue( ":fr", $fr, PDO::PARAM_STR);
    $st->bindValue( ":ar", $ar, PDO::PARAM_STR);
    $st->bindValue( ":es", $es, PDO::PARAM_STR);
    $st->bindValue( ":de", $de, PDO::PARAM_STR);
    $st->bindValue( ":ko", $ko, PDO::PARAM_STR);
    $st->bindValue( ":ru", $ru, PDO::PARAM_STR);
    $st->bindValue( ":pt", $pt, PDO::PARAM_STR);
    $st->bindValue( ":email", 'admin', PDO::PARAM_STR );
    $st->execute();





//$conn = null;

echo returnStatus(1 , 'new dict created',array('id' => $uuid));

}

//echo returnStatus(1 , 'new dict created',array('id' => $uuid));
?>
