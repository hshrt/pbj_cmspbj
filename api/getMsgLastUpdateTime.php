<?php

/*called by BPC server*/

ini_set( "display_errors", true );
require("../php/func_nx.php");
require("../config.php");
require("../php/inc.appvars.php");

session_start();

$roomId = isset($_REQUEST['roomId'])?$_REQUEST['roomId']:null;

if(strlen($roomId)==4 && substr($roomId,0,1)=="0"){
    $roomId = substr($roomId,1,3);
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select Max(matched.lastUpdate)as messageLastUpdate ,matched.source as source from (select rmm.*,m.source from
roomMessageMap rmm
inner join allroom g on g.room = :roomId && :roomId = rmm.room
inner join message m on rmm.messageId = m.id) as matched group by messageId order by messageLastUpdate DESC ";

$st = $conn->prepare ( $sql );

$st->bindValue( ":roomId", $roomId, PDO::PARAM_STR );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

//pprint_r($list[0]["messageLastUpdate"]);

$sql = "select Max(m.lastUpdate) as messageLastUpdate,m.source from message m where boardcast = 1";
$st = $conn->prepare ( $sql );

$st->execute();

$list_boardcast = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list_boardcast[] = $row;
    //echo json_encode($row);
}

//pprint_r($list_boardcast[0]["messageLastUpdate"]);

$return_result = null;

//echo(sizeof($list));
//echo(sizeof($list_boardcast));

if((sizeof($list)>0 && sizeof($list_boardcast)>0) && ($list[0]["messageLastUpdate"]!= null || $list_boardcast[0]["messageLastUpdate"]!=null)) {
    if (strtotime($list[0]["messageLastUpdate"]) > strtotime($list_boardcast[0]["messageLastUpdate"])) {
        $return_result = array($list[0]);
    } else {
        $return_result = $list_boardcast;
    }
}



if($return_result != null)
    echo returnStatus(1 , 'Get message LastUpdateTime ok!',$return_result);
else
    echo returnStatus(0 , 'Get message allLastUpdateTime fail!');

$conn = null;

?>
