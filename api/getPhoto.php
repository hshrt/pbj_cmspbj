<?php

//ini_set( "display_errors", true );
require("../config.php");
require("../php/inc.appvars.php");
require("../php/func_nx.php");


if(isset($_REQUEST['itemId']))
    $itemId= $_REQUEST['itemId'];

if(isset($_REQUEST['page'])){
    $page = $_REQUEST['page'];
}
if(isset($_REQUEST['itemPerPage'])){
    $itemPerPage = $_REQUEST['itemPerPage'];
}
$itemPerPage = isset($_REQUEST["itemPerPage"])?$_REQUEST["itemPerPage"]:20;

if(isset($_REQUEST['getCount'])){
    $getCount = $_REQUEST['getCount'];
}
if(isset($_REQUEST['getAllName'])){
    $getAllName = $_REQUEST['getAllName'];
}


if (empty($itemId)  && !empty($page) && $page < 0){
    echo returnStatus(0, 'miss itemId');
    exit;
}else{

    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    if(!empty($getAllName)){
        $sql = " select distinct fileName as fn ,fileExt from media t1 inner join mediaItemMap t2 on t1.id = t2.mediaId";
        $st = $conn->prepare ( $sql );
    }
    //this is for get the totalNumber of Media
    else if(!empty($getCount)){
        $sql = "select  count(*) as totalMediaNum from media where media.delete = 0";
        $st = $conn->prepare ( $sql );
    }
    else if(!empty($itemId)){
        $sql = "select id, filename as image, prefer, isIcon, media.fileExt from media, mediaItemMap where itemId = :itemId and id =
mediaId
and media.delete!= 1 order by lastUpdateTime DESC";
        $st = $conn->prepare ( $sql );
        $st->bindValue( ":itemId", $itemId, PDO::PARAM_STR );
    }
    else if(empty($itemId) && empty($getAllName)){
        $sql = "select  id,filename as image ,fileExt from media where media.delete = 0 order by fileExt ASC, uploadTime DESC limit
:page
 ,20";
        $st = $conn->prepare ( $sql );
        $st->bindValue( ":page", $page*$itemPerPage, PDO::PARAM_INT );
    }

    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
        //echo json_encode($row);
    }

    if(empty($getCount)) {
        for ($x = 0; $x < sizeof($list); $x++) {
            try {
                $size;
                $extension = 'jpg';
                if($list[$x]['fileExt']=='j'){
                    $extension = 'jpg';
                }
                else if($list[$x]['fileExt']=='p'){
                    $extension = 'png';
                }
                if (isset($list[$x]['image']) && $list[$x]['image'] != null) {
                    $size = getimagesize('../upload/' . $list[$x]['image'] . '_s.'.$extension);
                }
                else if(isset($list[$x]['fn']) && $list[$x]['fn'] != null) {
                    $size = getimagesize('../upload/' . $list[$x]['fn'] . '_s.'.$extension);
                }

                $list[$x]['width'] = $size[0];
                $list[$x]['height'] = $size[1];
            }
            catch(Exception $e){
                $list[$x]['width'] = 100;
                $list[$x]['height'] = 100;
            }
        }
    }

    $conn = null;

    echo returnStatus(1 , 'good',$list);
}


?>
