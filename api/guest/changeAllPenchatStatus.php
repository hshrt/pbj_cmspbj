<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$penchatStatus = isset($_POST['penchatStatus'])?$_POST['penchatStatus']:null;


if ( empty($penchatStatus)) {
    echo returnStatus(0, 'missing penchat status');
    exit;
}


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

// get data from allroom table
$sql = "SELECT * from allroom";
$st = $conn->prepare ( $sql );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$mqtt_url = "http://localhost:6000/room/penchat_status";

for ($index = 0; $index < sizeof($list); $index++) {
	if ($list[$index]["enable_penchat"] != $penchatStatus) {	
		$ch = curl_init();

		$jsonParam = new stdClass();
		$jsonParam->room = $list[$index]['room'];
		$jsonParam->penchat = $penchatStatus;
		curl_setopt($ch, CURLOPT_URL, $mqtt_url);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
		curl_setopt($ch, CURLOPT_POST, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
		curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonParam));
		$result = curl_exec($ch);
	}
}

$sql = "UPDATE allroom SET enable_penchat = :penchatStatus";
$st = $conn->prepare ( $sql );

$st->bindValue( ":penchatStatus", $penchatStatus, PDO::PARAM_STR );


$st->execute();

echo returnStatus(1, 'update all ok');

$conn = null;

?>
