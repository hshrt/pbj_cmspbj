<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();

$room = isset($_POST['room'])?$_POST['room']:null;
$penchat = isset($_POST['penchat'])?$_POST['penchat']:null;

$mqtt_url = "http://localhost:6000/room/penchat_status";

if ( empty($room)){
    echo returnStatus(0, 'missing room number');
    exit;
}

if ( empty($penchat)) {
    echo returnStatus(0, 'missing penchat status');
    exit;
}


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE allroom SET enable_penchat = :penchat where room = :room";
$st = $conn->prepare ( $sql );

$st->bindValue( ":room", $room, PDO::PARAM_STR );
$st->bindValue( ":penchat", $penchat, PDO::PARAM_STR );


$st->execute();

echo returnStatus(1, 'update ok');

$ch = curl_init();

$jsonParam = new stdClass();
$jsonParam->room = $room;
$jsonParam->penchat = $penchat;
curl_setopt($ch, CURLOPT_URL, $mqtt_url);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($jsonParam));
$result = curl_exec($ch);

$conn = null;

?>
