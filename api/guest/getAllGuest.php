<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

/*if(isset($_POST['itemId']))
    $itemId= $_POST['itemId'];*/

$forMsg = isset($_POST['forMsg'])?$_POST['forMsg']:null;
$msgId = isset($_POST['msgId'])?$_POST['msgId']:null;

$checkedIn = isset($_POST['$checkedIn'])?$_POST['$checkedIn']:false;

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");


$sql = "select * from allroom where chkin = 1 order by room ASC";

if(!$checkedIn){
    $sql = "select * from allroom order by room ASC";
}

if($forMsg != null){
    $sql = "SELECT (CASE
            WHEN g2.room IN (select g.room from allroom g inner join roomMessageMap rmm on g.room = rmm.room && rmm
            .messageId
= '".$msgId."' group by g.room)
               THEN 1
               ELSE 0
       END )as haveThisMsg,g2.* from allroom g2  where chkin = 1 order by room ASC;";
}


$st = $conn->prepare ( $sql );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

$conn = null;

echo returnStatus(1 , 'success',$list);



?>
