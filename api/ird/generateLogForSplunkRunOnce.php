<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$file = '../../../../../log/splunk/irdlog.log';


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT room,
itemDict.en as item_en,  itemDict.zh_cn as item_cn,
case when itemDict.en like subDict.en then null else subDict.en end as subitem_en, case when itemDict.zh_cn like subDict.zh_cn then null else subDict.zh_cn end as subitem_zh_cn,
deliveryTime, orderTime, numOfGuest, quantity FROM BSPPBJ.orders
left join BSPPBJ.items as itemList on SUBSTRING_INDEX(SUBSTRING_INDEX(foodIdList, ',', 1), ',', -1) like itemList.id
left join BSPPBJ.dictionary as itemDict on itemList.titleId like itemDict.id
 
cross join BSPPBJ.items as subitem
left join BSPPBJ.dictionary as subDict on subitem.titleId like subDict.id
where find_in_set(subitem.id, foodIdList) > 0
 
and month(deliveryTime) > 10 and  month(deliveryTime) <= 11 and year(deliveryTime) = 2018 and status = 1 and
exists(select 1 from BSPPBJ.items where SUBSTRING_INDEX(SUBSTRING_INDEX(foodIdList, ',', 1), ',', -1) like BSPPBJ.items.id)
 
order by deliveryTime, item_en, case when item_en like subDict.en then 0 else 1 end limit 9999;";
$st = $conn->prepare ($sql);
$st->execute();

while ($row = $st->fetch(PDO::FETCH_ASSOC) ) {
      $subItem="notapplied";
 $subItemCN="notapplied";
       if ($row['subitem_en'] != "") {
            $subItem=$row['subitem_en'];
$subItemCN=$row['subitem_zh_cn'];
}
$logSplunk = $row['orderTime'] . " - topic=sta/service/ird,client=cpu" . $row['room'] . ",msgItem=" . $row['item_en'] . ",msgSubItem=" .$subItem .",msgItemCN=" . $row['item_cn'] .",msgSubItemCN=" . $subItemCN .",guest=" . $row['numOfGuest'] . ",qty=" . $row['quantity'] . ",orderTime=" . $row['orderTime'] . ",deliveryTime=" . $row['deliveryTime']."\n";       
        $stats123 = file_put_contents($file, $logSplunk, FILE_APPEND);		
}
return 0;

?>
