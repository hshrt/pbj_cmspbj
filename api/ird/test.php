<?php

//////////////////////////////////////////////////////////////////////
//PARA: Date Should In YYYY-MM-DD Format
//RESULT FORMAT:
// '%y Year %m Month %d Day %h Hours %i Minute %s Seconds'        =>  1 Year 3 Month 14 Day 11 Hours 49 Minute 36 Seconds
// '%y Year %m Month %d Day'                                    =>  1 Year 3 Month 14 Days
// '%m Month %d Day'                                            =>  3 Month 14 Day
// '%d Day %h Hours'                                            =>  14 Day 11 Hours
// '%d Day'                                                        =>  14 Days
// '%h Hours %i Minute %s Seconds'                                =>  11 Hours 49 Minute 36 Seconds
// '%i Minute %s Seconds'                                        =>  49 Minute 36 Seconds
// '%h Hours                                                    =>  11 Hours
// '%a Days                                                        =>  468 Days
//////////////////////////////////////////////////////////////////////

function dateDifference($date_1 , $date_2 , $differenceFormat = '%a' )
{
    $datetime1 = date_create($date_1);
    $datetime2 = date_create($date_2);
    
    $interval = date_diff($datetime1, $datetime2);
  
    $d_diff = $interval->format("%d") * 24 * 60; 
    $h_diff = $interval->format("%h") * 60;
    $m_diff = $interval->format("%i");
 
    return intval($h_diff) + intval($m_diff);
    
}


	$testtime=date("Y-m-d H:i", strtotime("2018-04-23 0:00"));
	$testtime1=date("Y-m-d H:i", strtotime("2018-04-22 23:00"));
	//$interval = date_diff( date("H:i"), $testtime )->format("%i");

	echo dateDifference($testtime1, $testtime, "%h %i");

?>


