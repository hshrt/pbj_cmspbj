<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 *
 * For detect if there is any new upload in the movies folder for PBJ
 */

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$hash1 = md5_file('../../movie/small1.png');
$hash2 = md5_file('../../movie/small2.png');

$hashKey = "";

$directory = "../../movie/";
$filecount = 0;
$files = glob($directory . "small*.png");
if ($files){
 $filecount = count($files);
}
#echo "There were $filecount files";

for($x = 1; $x<=$filecount; $x++){
    $hashKey .= md5_file('../../movie/small'.$x.'.png').",";
}

$list =  array();
$list[0] = $hashKey;
$list[1] = $filecount;

if(strlen($hashKey)>0)
    echo returnStatus(1 , 'get movie hash OK',$list);
else{
    echo returnStatus(0 , 'get movie hash fail');
}


return 0;

?>
