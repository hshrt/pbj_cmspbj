<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 5/4/2017
 * Time: 2:09 AM
 */

//this command ensure the above relative path would work
//chdir(__DIR__);

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/func_nx.php");
require("../../php/inc.appvars.php");


$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select fileName,fileExt from media where media.delete = 1";
$st = $conn->prepare ( $sql );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

pprint_r($list);

foreach ($list as $imageItem){
    $extension = "jpg";

    if($imageItem["fileExt"]=="p"){
        $extension = "png";

    }

    $path = "../../upload/";
    $fileName =  $path.$imageItem["fileName"].".".$extension;
    $fileMediumName = $path.$imageItem["fileName"]."_m".".".$extension;
    $fileSmallName = $path.$imageItem["fileName"]."_s".".".$extension;

    echo $fileName.PHP_EOL;

    echo PHP_EOL."unlink original result= " .unlink($fileName);
    echo PHP_EOL."unlink medium result= " .unlink($fileMediumName);
    echo PHP_EOL."unlink small result= " .unlink($fileSmallName);
}


shell_exec('mysql -u '.DB_USERNAME.' -p'.'"'.DB_PASSWORD.'"'.' '.DB_NAME. ' < clearDeletedImage.sql');
?>

