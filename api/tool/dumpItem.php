<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */
/*this API is  for doing items copy from one section to another section*/

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

define( "THE_ROOT_PATH", "http://192.168.5.220/cmsphk/");

session_start();

//$parentId is the id of the section object which contains the article
$parentId = isset($_REQUEST['parentId'])?$_REQUEST['parentId']:'';
//$finishId is the id of the section object which will collect dumped article
$finishId = isset($_REQUEST['finishId'])?$_REQUEST['finishId']:'';



$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");


$sql = "select * from items where parentId = :parentId";

$st = $conn->prepare ( $sql );

$st->bindValue( ":parentId", $parentId, PDO::PARAM_STR);

$st->execute();

$list = array();
while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}
echo json_encode($list);

for($x=0;$x<sizeof($list);$x++){

    $item = $list[$x];
    //pprint_r($item);
    pprint_r($item["id"]);
    pprint_r( $item["titleId"]);
    $twoId = addArticle("test", "choice", $finishId, "1");
    $titleId = explode(",", $twoId)[0];
    $desId = explode(",", $twoId)[1];
    $objId = explode(",", $twoId)[2];

    $sql = "select * from dictionary where id = :sourceTitleId";

    $st = $conn->prepare ( $sql );

    $st->bindValue( ":sourceTitleId", $item["titleId"], PDO::PARAM_STR);

    $st->execute();
    $dict = array();
    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $dict[] = $row;
        //echo json_encode($row);
    }

    echo $dict[0]["en"].PHP_EOL;

    $d = $dict[0];

    addDict($titleId, $d['en'], $d['zh_hk'], $d['zh_cn'], $d['jp'], $d['fr'], $d['ar'], $d['es'], $d['de'],
        $d['ko'], $d['ru'], $d['pt'], "");

}

function addArticle($title,$type,$parentId,$skipCreateDescription){
    //setup input parameter

    echo ('addArticle fire first!');
    $data = array ('title' => $title, 'type' => $type, 'parentId'=>$parentId, 'skipCreateDescription'=>$skipCreateDescription);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(THE_ROOT_PATH.'api/createNewItemDM.php', 'r', false, $context);

    $response = stream_get_contents($fp);

    pprint_r($response);

    $responseObj = json_decode($response,true);

    pprint_r($responseObj["msg"]);

    return $responseObj["msg"];
}

function addDict($title_id,$title_en,$title_zh_hk,$title_zh_cn,$title_jp,$title_fr,$title_ar,$title_es,$title_de,$title_ko,$title_ru,$title_pt,$title_tr){

    echo ('addDict fire!');
    $data = array ('title_id' => $title_id, 'title_en' => $title_en, 'title_zh_hk'=>$title_zh_hk, 'title_zh_cn'=>$title_zh_cn,'title_jp' => $title_jp,'title_fr' => $title_fr,'title_ar' => $title_ar,'title_es' => $title_es,'title_de' => $title_de,'title_ko' => $title_ko,'title_ru' => $title_ru,'title_pt' => $title_pt,'title_tr' => $title_tr);

    $data = http_build_query($data);

    $context_options = array (
        'http' => array (
            'method' => 'POST',
            'header'=> "Content-type: application/x-www-form-urlencoded\r\n"
                . "Content-Length: " . strlen($data) . "\r\n",
            'content' => $data
        )
    );

    $context = stream_context_create($context_options);
    $fp = fopen(THE_ROOT_PATH.'api/updateItemDictDM.php', 'r', false, $context);

    $response = stream_get_contents($fp);

    pprint_r($response);

    $responseObj = json_decode($response,true);

    pprint_r($responseObj["msg"]);
}

?>
