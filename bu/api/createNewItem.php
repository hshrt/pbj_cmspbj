<?php 

require("../config.php");
require("../php/inc.appvars.php");
require("../php/func_nx.php");

session_start();
include("checkSession.php");

$title = isset($_POST['title'])?$_POST['title']:null;
$type = isset($_POST['type'])?$_POST['type']:null;

$skipCreateDescription = 0;

if(isset($_POST['skipCreateDescription'])){
    $skipCreateDescription = $_POST['skipCreateDescription'];
}

$parentId = 0;

if(isset($_POST['parentId'])){
    $parentId = $_POST['parentId'];
}

if(empty($title) || empty($type)){
    echo returnStatus(Invalid_input , "All field cannot be empty.");
    exit;
}


// Does the Article object already have an ID?
//if ( !is_null( $this->id ) ) trigger_error ( "Article::insert(): Attempt to insert an Article object that already has its ID property set (to $this->id).", E_USER_ERROR );

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "SELECT UUID() AS UUID";
$st = $conn->prepare ( $sql );
$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
}

$uuid = $list[0]["UUID"];

$sql = "INSERT INTO dictionary (id,en,lastUpdate, lastUpdateBy) VALUES (:id,:title,now(),:email)";
$st = $conn->prepare ( $sql );
$st->bindValue( ":id", $uuid, PDO::PARAM_STR );
$st->bindValue( ":title", $title, PDO::PARAM_STR );
$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
$st->execute();
$titleId = $uuid;

$desId = 'descriptionIdTemp';

if(($type == "article" || $type == "item" || $type == "Restaurant") && $skipCreateDescription == 0){

    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid_des = $list[0]["UUID"];

    $sql = "INSERT INTO dictionary (id,en,lastUpdate, lastUpdateBy) VALUES (:id,:title,now(),:email)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid_des, PDO::PARAM_STR );
    $st->bindValue( ":title", "", PDO::PARAM_STR );
    $st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
    $st->execute();
    $desId = $uuid_des;
    //echo("desId = ".$desId);
}

$sql = "INSERT INTO items (items.id,titleId, descriptionId, type, parentId,lastUpdate ,lastUpdateBy,items.order) VALUES (UUID(),:titleId,:desId, :type, :parentId, CURRENT_TIMESTAMP,:email,1000000)";
$st = $conn->prepare ( $sql );
$st->bindValue( ":titleId", $titleId, PDO::PARAM_STR );
$st->bindValue( ":desId", $desId, PDO::PARAM_STR );
$st->bindValue( ":type", $type, PDO::PARAM_STR );
$st->bindValue( ":parentId", $parentId, PDO::PARAM_STR );
$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
$st->execute();
//$this->id = $conn->lastInsertId();
$conn = null;
//echo $sql;
//header( "Location: ../index.php" );
echo returnStatus(1 , 'good');
?>
