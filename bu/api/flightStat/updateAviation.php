<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();
include("../checkSession.php");

$itemId = isset($_POST['itemId'])?$_POST['itemId']:null;
$type = isset($_POST['type'])?$_POST['type']:null;
$name = isset($_POST['name'])?$_POST['name']:null;
$iata = isset($_POST['iata'])?$_POST['iata']:null;

if ( empty($type)){
    echo returnStatus(0, 'missing type ');
    exit;
}



$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

if($itemId!=null){
    $sql = "UPDATE ".$type." SET name=:name,IATA=:iata,lastUpdate=now(),lastUpdateBy=:lastUpdateBy where id = :itemId";
}
else{
    $sql = "INSERT INTO ".$type. " (name, IATA,lastUpdate,
            lastUpdateBy) VALUES (:name,:iata,now(),
            :lastUpdateBy)";
}

$st = $conn->prepare ( $sql );

if($itemId!=null){
    $st->bindValue( ":name", $name, PDO::PARAM_STR );
    $st->bindValue( ":iata", $iata, PDO::PARAM_STR );
    $st->bindValue( ":itemId", $itemId, PDO::PARAM_STR );
    $st->bindValue( ":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR );
}
else{
    $st->bindValue( ":name", $name, PDO::PARAM_STR );
    $st->bindValue( ":iata", $iata, PDO::PARAM_STR );
    $st->bindValue( ":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR );
}

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'update ok!');
else
    echo returnStatus(0 , 'update fail! May be there is no change?');

$conn = null;

?>
