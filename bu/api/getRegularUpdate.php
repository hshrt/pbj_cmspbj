<?php

ini_set( "display_errors", true );
require("../php/func_nx.php");
require("../config.php");
require("../php/inc.appvars.php");

session_start();

$roomId = isset($_REQUEST['roomId'])?$_REQUEST['roomId']:null;

$lang = isset($_REQUEST['lang'])?$_REQUEST['lang']:"E";

$currentTime = isset($_REQUEST['currentTime'])?$_REQUEST['currentTime']:null;

switch($lang){
    case 'E' : $lang = 'en';break;
    case 'F' : $lang = 'fr';break;
    case 'CT' : $lang = 'zh_hk';break;
    case 'CS' : $lang = 'zh_cn';break;
    case 'J' : $lang = 'jp';break;
    case 'G' : $lang = 'de';break;
    case 'K' : $lang = 'ko';break;
    case 'R' : $lang = 'ru';break;
    case 'P' : $lang = 'pt';break;
    case 'A' : $lang = 'ar';break;
    case 'S' : $lang = 'es';break;

    default:  $lang = 'en';break;
}


if(strlen($roomId)==4 && substr($roomId,0,1)=="0"){
    $roomId = substr($roomId,1,3);
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "select Max(matched.lastUpdate)as messageLastUpdate ,matched.source as source from (select rmm.*,m.source from
roomMessageMap rmm
inner join allroom g on g.room = :roomId && :roomId = rmm.room
inner join message m on rmm.messageId = m.id) as matched group by messageId order by messageLastUpdate DESC ";

$st = $conn->prepare ( $sql );

$st->bindValue( ":roomId", $roomId, PDO::PARAM_STR );

$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}

//pprint_r($list[0]["messageLastUpdate"]);

$sql = "select Max(m.lastUpdate) as messageLastUpdate,m.source from message m where boardcast = 1";
$st = $conn->prepare ( $sql );

$st->execute();

$list_boardcast = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list_boardcast[] = $row;
    //echo json_encode($row);
}
$sql =
    "select distinct m.*,
    rmm.read,
d1.".$lang." as subject,
d2.".$lang." as description
from message m
    inner join roomMessageMap rmm ON (m.id = rmm.messageId && rmm.room =:roomId && m.id = rmm.messageId && m.boardcast = 2) && m.startDate <= :currentDate && (m
.endDate
>= :currentDate || m.endDate = '0000-00-00 00:00:00')
    inner join dictionary_msg d1 on d1.id = m.subjectId && m.startDate <= :currentDate && (m.endDate
>= :currentDate || m.endDate = '0000-00-00 00:00:00') && m.status !='D' && m.boardcast = 2
    inner join dictionary_msg d2 on d2.id = m.descriptionId
    order by lastUpdate DESC";

$st = $conn->prepare ( $sql );

$st->bindVAlue( ":roomId", $roomId, PDO::PARAM_STR);
$st->bindVAlue( ":currentDate", $currentTime, PDO::PARAM_STR);

$st->execute();

$list_msg = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list_msg[] = $row;
    //echo json_encode($row);
}

//pprint_r($list_boardcast[0]["messageLastUpdate"]);

$return_result = null;



if((sizeof($list)>0 && sizeof($list_boardcast)>0) && ($list[0]["messageLastUpdate"]!= null || $list_boardcast[0]["messageLastUpdate"]!=null)) {
    if (strtotime($list[0]["messageLastUpdate"]) > strtotime($list_boardcast[0]["messageLastUpdate"])) {

        $return_result = array('message'=>$list[0]);
    } else {

        $return_result = $list_boardcast;
        $return_result = array('message'=>$list_boardcast[0]);
    }
}

$return_result = $list_boardcast;
$return_result = array('message'=>$list_boardcast[0]);

$return_result['boardcastMessage'] = $list_msg;

function returnStatus2( $status, $msg,  $data_array){
    $out = array(
        'status' => $status,
        'msg' => $msg,
        'data' => $data_array
    );

    return json_encode($out);
}


if($return_result != null) {

    echo returnStatus2(1, 'Get message LastUpdateTime ok!', $return_result);
}




/*else
    echo returnStatus(0 , 'Get message allLastUpdateTime fail!');*/

$conn = null;

?>
