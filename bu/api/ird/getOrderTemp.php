<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$date = isset($_REQUEST['date'])?$_REQUEST['date']:'';

//filter by status
$status = isset($_REQUEST['status'])?$_REQUEST['status']:0;




if ( 0){
    echo returnStatus(0, 'missing room number');
}

else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");


    $sql = "select id,room,foodIdList, deliveryTime, orderTime, numOfGuest,quantity,status from orders ";

    if(isset($_REQUEST["date"])){
        $sql = $sql." where orderTime >:date1 && 
orderTime < DATE
(DATE_ADD(:date1, interval 1 
        day))";
    }

    if(isset($_REQUEST["status"])){
        $sql = $sql." && status = :status ";
    }

    $orderBy = "order by orderTime DESC";

    $sql = $sql.$orderBy;

    $st = $conn->prepare ( $sql );
    $st->bindValue( ":date1",$date, PDO::PARAM_STR );
    $st->bindValue( ":status",$status, PDO::PARAM_INT );

    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    if($st->fetchColumn() > 0 || $st->rowCount() > 0){

        echo returnStatus(1, 'get Order OK',$list);
    }
    else{
        echo returnStatus(0, 'get Order fail',$list);
    }
}
return 0;

?>
