<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$date = isset($_REQUEST['date'])?$_REQUEST['date']:'';

//filter by status
$status = isset($_REQUEST['status'])&& strlen($_REQUEST['status']) > 0?$_REQUEST['status']:null;

$room = isset($_REQUEST['room'])&& strlen($_REQUEST['room']) > 0?$_REQUEST['room']:null;


if ( 0){
    echo returnStatus(0, 'missing room number');
}

else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");


    $sql = "select id,room,foodIdList, deliveryTime, orderTime, numOfGuest,quantity,status from orders ";

    if(isset($_REQUEST["date"])){
        $sql = $sql." where orderTime >:date1 && 
orderTime < DATE
(DATE_ADD(:date1, interval 1 
        day))";
    }

    if(isset($_REQUEST["status"])&& strlen($_REQUEST['status']) > 0){
        $sql = $sql." && status = :status ";
    }
    if($room!=null){
        $sql = $sql." && room = :room ";
    }
    
    $orderBy = "order by orderTime DESC";
    
    $sql = $sql.$orderBy;
    
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":date1",$date, PDO::PARAM_STR );
    if(isset($_REQUEST["status"])&& strlen($_REQUEST['status']) > 0) {
        $st->bindValue(":status", $status, PDO::PARAM_INT);
    }
    if($room!=null) {
        $st->bindValue(":room", $room, PDO::PARAM_STR); 
    }

    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }
    for($x=0;$x< sizeof($list);$x++){
        $foodIdList = explode(',', $list[$x]["foodIdList"]);
        //pprint_r($foodIdList);
        for($y=0; $y< sizeof($foodIdList); $y++){


            $sql = "select d.en from dictionary d inner join
                        items i on i.titleId=d.id && i.id=:foodId";


            $st = $conn->prepare ( $sql );
            $st->bindValue( ":foodId",$foodIdList[$y], PDO::PARAM_STR );

            $st->execute();

            $list2 = array();
            while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
                $list2[] = $row;
            }
            //pprint_r($list2);
            if($y==0) {
                //pprint_r($list2);

                if(sizeof($list2)>0) {
                    $list[$x]["item"] = $list2[0]["en"];
                }
            }
            else if ($y==1){
                if(sizeof($list2)>0) {
                    $list[$x]["choices"] = $list2[0]["en"];
                }
            }
            else{
                if(sizeof($list2)>0) {
                    $list[$x]["choices"] = $list[$x]["choices"] . ", " . $list2[0]["en"];
                }
            }

            unset($list[$x]["foodIdList"]);
        }
    }

    if($st->fetchColumn() > 0 || $st->rowCount() > 0){

        echo returnStatus(1, 'get Order OK',$list);
    }
    else{
        echo returnStatus(0, 'get Order fail',$list);
    }
}

return 0;

?>
