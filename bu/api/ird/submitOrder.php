<?php
/**
 * Created by PhpStorm.
 * User: marcopo
 * Date: 8/10/2016
 * Time: 3:19 PM
 */


ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$room = isset($_REQUEST['room'])?$_REQUEST['room']:'';
$foodIdList = isset($_REQUEST['foodIdList'])?$_REQUEST['foodIdList']:'';
$orderTime= isset($_REQUEST['orderTime'])?$_REQUEST['orderTime']:'';
$deliveryTime= isset($_REQUEST['deliveryTime'])?$_REQUEST['deliveryTime']:'';
$numOfGuest = isset($_REQUEST['numOfGuest'])?$_REQUEST['numOfGuest']:1;
$quantity = isset($_REQUEST['quantity'])?$_REQUEST['quantity']:'';
$status = isset($_REQUEST['status'])?$_REQUEST['status']:0;
$lastUpdateBy = isset($_REQUEST['lastUpdateBy'])?$_REQUEST['lastUpdateBy']:'BSP';



if ( empty($room)){
    echo returnStatus(0, 'missing room number');
}
else if(strlen($foodIdList) == 0){
    echo returnStatus(0, 'The food Id List is empty.');
}
else if(strlen($deliveryTime) == 0){
    echo returnStatus(0, 'The delivery time is empty');
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    //*****create Order
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid = $list[0]["UUID"];

    $sql = "INSERT INTO orders (id,room, foodIdList, deliveryTime, orderTime, numOfGuest,quantity, status, lastUpdate, 
lastUpdateBy) 
VALUES 
(:id,
:room,:foodIdList,:deliveryTime,:orderTime,:numOfGuest,:quantity,:status,now(),:lastUpdateBy)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
    $st->bindValue( ":room",$room, PDO::PARAM_STR );
    $st->bindValue( ":foodIdList",$foodIdList, PDO::PARAM_STR );
    $st->bindValue( ":deliveryTime",$deliveryTime, PDO::PARAM_STR );
    $st->bindValue( ":orderTime",$orderTime, PDO::PARAM_STR );
    $st->bindValue( ":numOfGuest",$numOfGuest, PDO::PARAM_INT );
    $st->bindValue( ":quantity",$quantity, PDO::PARAM_INT );
    $st->bindValue( ":status",$status, PDO::PARAM_INT );
    $st->bindValue( ":lastUpdateBy",$lastUpdateBy, PDO::PARAM_INT );
    $st->execute();


    if($st->fetchColumn() > 0 || $st->rowCount() > 0){

        echo returnStatus(1, 'Order submission OK');
    }
    else{
        echo returnStatus(0, 'Order submission fail');
    }
}
return 0;

?>
