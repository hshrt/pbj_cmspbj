<?php

/**used by parseTC3.php to add Opera Message */

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

session_start();

$roomNum = isset($_POST['roomNum'])?$_POST['roomNum']:'';
$messageId= isset($_POST['messageId'])?$_POST['messageId']:'';//not use for new approach
$startDate= isset($_POST['startDate'])?$_POST['startDate']:'';
$endDate = isset($_POST['endDate'])?$_POST['endDate']:'';
$msg = isset($_POST['msg'])?$_POST['msg']:'';

$subject = isset($_POST['subject'])?$_POST['subject']:'Guest Message';
$source = isset($_POST['source'])?$_POST['source']:'';
$status = isset($_POST['status'])?$_POST['status']:'';

$type = isset($_POST['type'])?$_POST['type']:0;//not use for new approach

$lastUpdateBy = isset($_POST['lastUpdateBy'])?$_POST['lastUpdateBy']:'';

if ( empty($roomNum)){
    echo returnStatus(0, 'missing room number');
}
else if(strlen($msg) == 0){
    echo returnStatus(0, 'The message is empty, we will not move on.');
}
else if(strlen($msg) == 0){
    echo returnStatus(0, 'The message is empty, we will not move on.');
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    //*****create Dictionary for Subject
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid = $list[0]["UUID"];

    $sql = "INSERT INTO dictionary_msg (id,en,zh_hk,zh_cn,jp,fr,ar,es,de,ko,ru,pt,lastUpdate, lastUpdateBy) VALUES (:id,
:subject,:subject,:subject,:subject,:subject,:subject,:subject,:subject,:subject,:subject,:subject,now(),:email)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
    $st->bindValue( ":subject", $subject, PDO::PARAM_STR );
    $st->bindValue( ":email", "TC3", PDO::PARAM_STR );
    $st->execute();
    $titleId = $uuid;

    //******create dictionary for Description
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid_des = $list[0]["UUID"];

    $sql = "INSERT INTO dictionary_msg (id,en,zh_hk,zh_cn,jp,fr,ar,es,de,ko,ru,pt,lastUpdate, lastUpdateBy) VALUES (:id,
:description,:description,:description,:description,:description,:description,:description,:description,:description,:description,:description,now(),:email)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid_des, PDO::PARAM_STR );
    $st->bindValue( ":description", $msg, PDO::PARAM_STR );
    $st->bindValue( ":email", "TC3", PDO::PARAM_STR );
    $st->execute();
    $desId = $uuid_des;

    echo"&&&&&&&&&&&&&&&&&&&&&&  msg = ".$msg."</br>";

    //create a unique ID  for message**
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }
    $uuid_msg = $list[0]["UUID"];
    //**

    echo "tester tester tester";

    $sql = "INSERT INTO message (id, subjectId, descriptionId, source,status, startDate, endDate, lastUpdate, lastUpdateBy)
VALUES (:id,:subjectId,:descriptionId,:source,:status,:startDate,:endDate,now(),:lastUpdateBy)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid_msg, PDO::PARAM_STR );
    $st->bindValue( ":subjectId", $titleId, PDO::PARAM_STR);
    $st->bindValue( ":descriptionId", $desId, PDO::PARAM_STR );
    $st->bindValue( ":source", $source, PDO::PARAM_STR );
    $st->bindValue( ":status", 'unread', PDO::PARAM_STR );
    $st->bindValue( ":startDate", $startDate, PDO::PARAM_STR );
    $st->bindValue( ":endDate", $endDate, PDO::PARAM_STR );
    $st->bindValue( ":lastUpdateBy",$lastUpdateBy , PDO::PARAM_STR );
    //$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR );
    $st->execute();


    if($st->rowCount() > 0){

        $messageId = $uuid_msg;
        echo "damn damn";
        //make guest and message mapping**
        $sql = "INSERT INTO roomMessageMap (room, messageId, lastUpdate, lastUpdateBy ) VALUES (:room, :messageId, now(),
:lastUpdateBy)";

        $st_guest = $conn->prepare ($sql);

        $st_guest->bindVAlue( ":room", $roomNum, PDO::PARAM_STR);
        $st_guest->bindVAlue( ":messageId", $messageId, PDO::PARAM_STR);
        $st_guest->bindVAlue( ":lastUpdateBy", "TC3", PDO::PARAM_STR);


        $st_guest->execute();

        echo returnStatus(1, 'Add message OK');
    }
    else{
        echo returnStatus(0, 'Add message fail');
    }
}
return 0;

?>
