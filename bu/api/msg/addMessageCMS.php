<?php

ini_set( "display_errors", true );
require("../../config.php");

require("../../php/inc.appvars.php");
require("../../php/func_nx.php");

$forFax = isset($_POST["forFax"])?true:false;

session_start();

if(!$forFax) {
    include("../checkSession.php");
}

$title_en =  isset($_POST["title_en"])?$_POST["title_en"]:"";
$title_zh_hk = isset($_POST["title_zh_hk"])?$_POST["title_zh_hk"]:$title_en;
$title_zh_cn = isset($_POST["title_zh_cn"])?$_POST["title_zh_cn"]:$title_en;
$title_jp = isset($_POST["title_jp"])?$_POST["title_jp"]:$title_en;
$title_fr = isset($_POST["title_fr"])?$_POST["title_fr"]:$title_en;
$title_ar = isset($_POST["title_ar"])?$_POST["title_ar"]:$title_en;
$title_es = isset($_POST["title_es"])?$_POST["title_es"]:$title_en;
$title_de = isset($_POST["title_de"])?$_POST["title_de"]:$title_en;
$title_ko = isset($_POST["title_ko"])?$_POST["title_ko"]:$title_en;
$title_ru = isset($_POST["title_ru"])?$_POST["title_ru"]:$title_en;
$title_pt = isset($_POST["title_pt"])?$_POST["title_pt"]:$title_en;

$des_en = isset($_POST["des_en"])?$_POST["des_en"]:"";
$des_zh_hk = isset($_POST["des_zh_hk"])?$_POST["des_zh_hk"]:$des_en;
$des_zh_cn = isset($_POST["des_zh_cn"])?$_POST["des_zh_cn"]:$des_en;
$des_jp = isset($_POST["des_jp"])?$_POST["des_jp"]:$des_en;
$des_fr = isset($_POST["des_fr"])?$_POST["des_fr"]:$des_en;
$des_ar = isset($_POST["des_ar"])?$_POST["des_ar"]:$des_en;
$des_es = isset($_POST["des_es"])?$_POST["des_es"]:$des_en;
$des_de = isset($_POST["des_de"])?$_POST["des_de"]:$des_en;
$des_ko = isset($_POST["des_ko"])?$_POST["des_ko"]:$des_en;
$des_ru = isset($_POST["des_ru"])?$_POST["des_ru"]:$des_en;
$des_pt = isset($_POST["des_pt"])?$_POST["des_pt"]:$des_en;



$startDate= isset($_POST['startDate'])?$_POST['startDate']:'';
$endDate = isset($_POST['endDate'])?$_POST['endDate']:'';

$source = isset($_POST['source'])?$_POST['source']:'';
$status = isset($_POST['status'])?$_POST['status']:'';

$priority = isset($_POST['priority'])?$_POST['priority']:0;
$boardcast = isset($_POST['boardcast'])?$_POST['boardcast']:0;


$guestMsgIdList = isset($_POST['guestMsgIdList'])?$_POST['guestMsgIdList']:null;
$guestMsgIdList = explode(",", $guestMsgIdList);

$type = isset($_POST['type'])?$_POST['type']:0;

if(strlen($title_en) == 0){
    echo returnStatus(0, 'The title is empty, we will not move on.');
}
else{
    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    //*****create Dictionary for Subject
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid = $list[0]["UUID"];

    $sql = "INSERT INTO dictionary_msg (id,en,zh_hk,zh_cn,jp,fr,ar,es,de,ko,ru,pt,lastUpdate, lastUpdateBy,type) VALUES (:id,
:title_en,:title_zh_hk,:title_zh_cn,:title_jp,:title_fr,:title_ar,:title_es,:title_de,:title_ko,:title_ru,:title_pt,now(),:email,:types)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
    $st->bindValue( ":title_en", $title_en, PDO::PARAM_STR );
    $st->bindValue( ":title_zh_hk", $title_zh_hk, PDO::PARAM_STR );
    $st->bindValue( ":title_zh_cn", $title_zh_cn, PDO::PARAM_STR );
    $st->bindValue( ":title_jp", $title_jp, PDO::PARAM_STR );
    $st->bindValue( ":title_fr", $title_fr, PDO::PARAM_STR );
    $st->bindValue( ":title_ar", $title_ar, PDO::PARAM_STR );
    $st->bindValue( ":title_es", $title_es, PDO::PARAM_STR );
    $st->bindValue( ":title_de", $title_de, PDO::PARAM_STR );
    $st->bindValue( ":title_ko", $title_ko, PDO::PARAM_STR );
    $st->bindValue( ":title_ru", $title_ru, PDO::PARAM_STR );
    $st->bindValue( ":title_pt", $title_pt, PDO::PARAM_STR );

    $email = $forFax?"FAX":$_SESSION['email'];

    $st->bindValue( ":email", $email, PDO::PARAM_STR );
    $st->bindValue( ":types", $type, PDO::PARAM_STR );
    $st->execute();
    $titleId = $uuid;

    //******create dictionary for Description
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid_des = $list[0]["UUID"];

    $sql = "INSERT INTO dictionary_msg (id,en,zh_hk,zh_cn,jp,fr,ar,es,de,ko,ru,pt,lastUpdate, lastUpdateBy,type) VALUES (:id,
:des_en,:des_zh_hk,:des_zh_cn,:des_jp,:des_fr,:des_ar,:des_es,:des_de,:des_ko,:des_ru,:des_pt,now(),:email,:types)";
    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid_des, PDO::PARAM_STR );
    $st->bindValue( ":des_en", $des_en, PDO::PARAM_STR );
    $st->bindValue( ":des_zh_hk", $des_zh_hk, PDO::PARAM_STR );
    $st->bindValue( ":des_zh_cn", $des_zh_cn, PDO::PARAM_STR );
    $st->bindValue( ":des_jp", $des_jp, PDO::PARAM_STR );
    $st->bindValue( ":des_fr", $des_fr, PDO::PARAM_STR );
    $st->bindValue( ":des_ar", $des_ar, PDO::PARAM_STR );
    $st->bindValue( ":des_es", $des_es, PDO::PARAM_STR );
    $st->bindValue( ":des_de", $des_de, PDO::PARAM_STR );
    $st->bindValue( ":des_ko", $des_ko, PDO::PARAM_STR );
    $st->bindValue( ":des_ru", $des_ru, PDO::PARAM_STR );
    $st->bindValue( ":des_pt", $des_pt, PDO::PARAM_STR );

    $email = $forFax?"FAX":$_SESSION['email'];

    $st->bindValue( ":email", $email, PDO::PARAM_STR );
    $st->bindValue( ":types", $type, PDO::PARAM_STR);
    $st->execute();
    $desId = $uuid_des;

    //echo"&&&&&&&&&&&&&&&&&&&&&&  msg = ".$msg."</br>";

    //create a unique ID  for message**
    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }
    $uuid_msg = $list[0]["UUID"];
    //**

    $sql = "INSERT INTO message (id, subjectId, descriptionId, source,status, startDate, endDate,priority, boardcast, lastUpdate, lastUpdateBy) VALUES (:id,:subjectId,:descriptionId,:source,:status,:startDate,:endDate,:priority, :boardcast, now(),:lastUpdateBy)";


    $st = $conn->prepare ( $sql );
    $st->bindValue( ":id", $uuid_msg, PDO::PARAM_STR );
    $st->bindValue( ":subjectId", $titleId, PDO::PARAM_STR );
    $st->bindValue( ":descriptionId", $desId, PDO::PARAM_STR );
    $st->bindValue( ":source", $source, PDO::PARAM_STR );
    $st->bindValue( ":status", 'unread', PDO::PARAM_STR );
    $st->bindValue( ":startDate", $startDate, PDO::PARAM_STR );
    $st->bindValue( ":endDate", $endDate, PDO::PARAM_STR );
    $st->bindValue( ":priority", $priority, PDO::PARAM_STR );
    $st->bindValue( ":boardcast", $boardcast, PDO::PARAM_STR );

    $email = $forFax?"FAX":$_SESSION['email'];

    $st->bindValue( ":lastUpdateBy",$email , PDO::PARAM_STR );
    $st->execute();


    if($st->rowCount() > 0){

        //make guest and message mapping*

        $list = array();

        $list = $guestMsgIdList;

        //echo "size of guest list = " .sizeof($list);

        for($x=0;$x<sizeof($list);$x++){
            $room = $list[$x];
            $messageId = $uuid_msg;

            //make guest and message mapping**
            $sql = "INSERT INTO roomMessageMap (room, messageId, lastUpdate, lastUpdateBy ) VALUES (:room, :messageId, now(),
:lastUpdateBy)";

            $st_guest = $conn->prepare ($sql);

            $st_guest->bindVAlue( ":room", $room, PDO::PARAM_STR);
            $st_guest->bindVAlue( ":messageId", $messageId, PDO::PARAM_STR);

            $email = $forFax?"FAX":$_SESSION['email'];
            $st_guest->bindVAlue( ":lastUpdateBy", $email, PDO::PARAM_STR);


            $st_guest->execute();
        }

        echo returnStatus(1, 'Add message CMS OK');
    }
    else{
        echo returnStatus(0, 'Add message CMS fail');
    }
}
return 0;

?>
