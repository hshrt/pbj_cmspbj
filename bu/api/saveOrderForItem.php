<?php

ini_set( "display_errors", true );
require( "../config.php" );

require("../php/inc.appvars.php");

session_start();
include("checkSession.php");

$orderArr = json_decode($_POST['orderArr']);
$idArr= explode(",",$_POST['idArr']);



if ( empty($idArr) || empty($orderArr)){
    echo returnStatus(0, 'missing input');
    exit;
}else{

    $conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
    $conn->exec("set names utf8");

    $sql = "UPDATE items SET items.lastUpdate = now(),items.order = CASE id ";
    $csv = "";

    for ($x = 0;$x< sizeof($idArr);$x++) {
        $sql .= sprintf("WHEN '%s' THEN %d ", $idArr[$x], $orderArr[$x]);
        $csv .= sprintf("'%s',", $idArr[$x]);
    }
    $csv = substr($csv,0, -1);
    //$csv = implode(',', $idArr);
    $sql .= "END WHERE id IN ($csv)";

    //echo($sql);
   // exit;
    //$sql = "INSERT INTO mediaItemMap (id,mediaId,itemId,lastupdateTime ) VALUES (UUID() ,:mediaId, :itemId, now())";
    $st = $conn->prepare ( $sql );

    $st->execute();

    echo returnStatus(1 , 'good');
}


?>
