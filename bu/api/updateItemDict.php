<?php 

require( "../config.php" );
require("../php/inc.appvars.php");

session_start();
include("checkSession.php");


$title_id= $_POST["title_id"];
$title_en = $_POST["title_en"];
$title_zh_hk = $_POST["title_zh_hk"];
$title_zh_cn = $_POST["title_zh_cn"];
$title_jp = $_POST["title_jp"];
$title_fr = $_POST["title_fr"];
$title_ar = $_POST["title_ar"];
$title_es = $_POST["title_es"];
$title_de = $_POST["title_de"];
$title_ko = $_POST["title_ko"];
$title_ru = $_POST["title_ru"];
$title_pt = $_POST["title_pt"];

$forMsg = isset($_POST["forMsg"])?$_POST["forMsg"]:0;

// Insert the Article
$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD );
$conn->exec("set names utf8");

$sql = "UPDATE dictionary SET en=:en,zh_hk=:zh_hk, zh_cn=:zh_cn, jp=:jp, fr=:fr, ar=:ar, es=:es, de=:de, ko=:ko, ru=:ru, pt=:pt ,lastUpdate=now(),lastUpdateBy=:email where id = :id";

if($forMsg == 1){
    $sql = "UPDATE dictionary_msg SET en=:en,zh_hk=:zh_hk, zh_cn=:zh_cn, jp=:jp, fr=:fr, ar=:ar, es=:es, de=:de, ko=:ko, ru=:ru,
pt=:pt ,lastUpdate=now(),lastUpdateBy=:email where id = :id";
}

$st = $conn->prepare ( $sql );
$st->bindValue( ":id", $title_id, PDO::PARAM_STR);
$st->bindValue( ":en", $title_en, PDO::PARAM_STR);
$st->bindValue( ":zh_hk", $title_zh_hk, PDO::PARAM_STR);
$st->bindValue( ":zh_cn", $title_zh_cn, PDO::PARAM_STR);
$st->bindValue( ":jp", $title_jp, PDO::PARAM_STR);
$st->bindValue( ":fr", $title_fr, PDO::PARAM_STR);
$st->bindValue( ":ar", $title_ar, PDO::PARAM_STR);
$st->bindValue( ":es", $title_es, PDO::PARAM_STR);
$st->bindValue( ":de", $title_de, PDO::PARAM_STR);
$st->bindValue( ":ko", $title_ko, PDO::PARAM_STR);
$st->bindValue( ":ru", $title_ru, PDO::PARAM_STR);
$st->bindValue( ":pt", $title_pt, PDO::PARAM_STR);
$st->bindValue( ":email", $_SESSION['email'], PDO::PARAM_STR);
$st->execute();

$conn = null;

if($st->fetchColumn() > 0 || $st->rowCount() > 0){
    echo returnStatus(1 , 'update item success!');
}
else{
    echo returnStatus(0 , 'update item fail ');
}


?>
