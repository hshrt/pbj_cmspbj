<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();
include("../checkSession.php");

$oldPassword = isset($_POST['oldPassword'])?$_POST['oldPassword']:null;
$newPassword = isset($_POST['newPassword'])?$_POST['newPassword']:null;
$userId = isset($_POST['userId'])?$_POST['userId']:null;


if ( empty($userId)&& empty($userId)){
    echo returnStatus(Invalid_input, 'missing userId');
    exit;
}

if ( empty($oldPassword)&& empty($oldPassword)){
    echo returnStatus(Invalid_input, 'missing old password');
    exit;
}
if ( empty($newPassword)&& empty($newPassword)){
    echo returnStatus(Invalid_input, 'missing new password');
    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

if($userId!=null){
    $sql = "SELECT * from user where id = :userId && pass = :oldPW";
    $st = $conn->prepare ( $sql );

    $st->bindValue( ":userId", $userId, PDO::PARAM_STR );
    $st->bindValue( ":oldPW", $oldPassword, PDO::PARAM_STR );


    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
        //echo json_encode($row);
    }

    //$conn = null;


    if($st->rowCount()  > 0 || sizeof($list) > 0) {
        $sql2 = "UPDATE user SET pass=:newPW where id=:userId";
        $st2 = $conn->prepare($sql2);

        $st2->bindValue( ":userId", $userId, PDO::PARAM_STR );
        $st2->bindValue( ":newPW", $newPassword, PDO::PARAM_STR );

        $st2->execute();

        $list2 = array();

        while ( $row2 = $st2->fetch(PDO::FETCH_ASSOC) ) {
            $list2[] = $row2;
            //echo json_encode($row);
        }
        if($st2->rowCount()  > 0) {
            echo returnStatus(1 , 'update ok!');
        }
    }
    else {
        echo returnStatus(Invalid_input, 'wrong password');
        exit;
    }
}

$conn = null;

?>
