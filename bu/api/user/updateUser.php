<?php

ini_set( "display_errors", true );
require( "../../config.php" );
require("../../php/inc.appvars.php");

session_start();
include("../checkSession.php");

$new = isset($_POST['new'])?$_POST['new']:null;
$userId = isset($_POST['userId'])?$_POST['userId']:null;
$firstName = isset($_POST['firstName'])?$_POST['firstName']:null;
$lastName = isset($_POST['lastName'])?$_POST['lastName']:null;
$email = isset($_POST['email'])?$_POST['email']:null;
$pass = isset($_POST['pass'])?$_POST['pass']:null;
$role = isset($_POST['role'])?$_POST['role']:null;

if ( empty($userId)&& empty($new)){
    echo returnStatus(Invalid_input, 'missing userId');
    exit;
}
if (empty($firstName) || empty($lastName) || (empty($userId)&&empty($pass)) || empty($email) || empty($role)){
    echo returnStatus(Invalid_input, 'missing required information');
    exit;
}

//email validation
if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
    $emailErr = "Invalid email format";
    echo returnStatus(Invalid_input, $emailErr);
    exit;
}

$conn = new PDO( DB_DSN, DB_USERNAME, DB_PASSWORD);
$conn->exec("set names utf8");

if($userId!=null){
    $sql = "UPDATE user SET firstName=:firstname,lastName=:lastname,email=:email,role=:role,lastUpdate=now(),
lastUpdateBy=:lastUpdateBy
where id = :userId";
}
else{

    $sql = "SELECT UUID() AS UUID";
    $st = $conn->prepare ( $sql );
    $st->execute();

    $list = array();

    while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
        $list[] = $row;
    }

    $uuid = $list[0]["UUID"];

    $sql = "INSERT INTO user (id,firstName, lastName,email,pass, role, lastUpdate,
            lastUpdateBy) VALUES (:id,:firstname,:lastname, :email,:pass, :role,now(),
            :lastUpdateBy)";
}

$st = $conn->prepare ( $sql );

if($userId!= null){
    $st->bindValue( ":userId", $userId, PDO::PARAM_STR );
}
else{
    $st->bindValue( ":id", $uuid, PDO::PARAM_STR );
    $st->bindValue( ":pass", $pass, PDO::PARAM_STR );
}


$st->bindValue( ":firstname", $firstName, PDO::PARAM_STR );
$st->bindValue( ":lastname", $lastName, PDO::PARAM_STR );
$st->bindValue( ":email", $email, PDO::PARAM_STR );
$st->bindValue( ":role", $role, PDO::PARAM_STR );
$st->bindValue( ":lastUpdateBy", $_SESSION['email'], PDO::PARAM_STR );


$st->execute();

$list = array();

while ( $row = $st->fetch(PDO::FETCH_ASSOC) ) {
    $list[] = $row;
    //echo json_encode($row);
}


if($st->rowCount()  > 0)
    echo returnStatus(1 , 'update ok!');
else
    echo returnStatus(0 , 'update fail! May be there is no change?');

$conn = null;

?>
