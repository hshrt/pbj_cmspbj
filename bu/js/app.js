var App = App || {};
var TIPS_BOX_SHOWN_TIME = 3000;

App.currentId = null;
App.currentItem = null;
App.imageData = null;
App.imageFileName = null;
App.idArray = new Array();
App.typeArray = new Array();
App.haveItem = false;
App.baseFolder = "cmspbj";
App.hotelLocation = "beijing";
App.serverType = "production";

App.strings = App.strings || {};
App.strings.sessionTimeOut = "Session is time-out, please login again.";



App.init = function(){
    console.log("jquery width" + $(window).width());
    console.log("window Innerwidth" + window.innerWidth);
    console.log("window outerwidth" + window.outerWidth);
    console.log("App init");


    App.subTopBar =  new App.SubTopBar();

    //App.startCheckSession();

    //App.subHead = new App.SubHead();
    //App.libList = new App.ItemList();
    $("#userEmail").text(App.userEmail);

    $("#changePW").hide();

    $("#mailContainer").hover(
        //on mouseover
        function() {
            if(App.userEmail != 'admin') {
                $("#changePW").show();
            }
            /*$(this).animate({
                    height: '+=60px' //adds 250px
                }, 'fast' //sets animation speed to slow
            );*/
        },
        //on mouseout
        function() {
            $("#changePW").hide();
            /*$(this).animate({
                    height: '-=60px' //substracts 250px
                }, 'fast'
            );*/
        }
    );
    $("#changePW").on("moustout", function(){
        //$("#changePW").hide();
    });

    $("#changePW").on("click", function(){
        var url = window.location.protocol+'//'+window.location.hostname+'/'+App.baseFolder+'/#/';

        url += "property/"+App.hotelLocation +"/changePW";
        window.location = document.URL = url;
    });

    $("#logoutBtn").on("mouseover",function(){
        //$("#logoutBtn").text("Logout");
    }).on("mouseout",function(){
        //$("#logoutBtn").text(App.userEmail);
    });

    //App.popupBox.setupHandler();
    $("#logoutBtn").on("click", function(){
        $.ajax({
            url : "api/logout.php",
            method : "POST",
            dataType: "json",
            data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(json){
            console.log(json);

            location.reload();

        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    });


}

App.goUpperLevel = function(){
    //window.location = document.URL = window.location.protocol+'//'+window.location.hostname+'/'+App.baseFolder+'/#/property/beverly-hill/'+typeObject.list[1];
    var param = $.address.pathNames();
    var url = window.location.protocol+'//'+window.location.hostname+'/'+App.baseFolder+'/#/';

    for(var x = 0;x < param.length-1;x++){
        url+=param[x];

        if(x != param.length-2){
            url+='/';
        }
    }
    window.location = document.URL = url;

}

App.showTipBox = function(mode,title,content){

    if(mode == 'suscess-mode'){
        $("#tipsBox").addClass("callout-success");
    }
    else if(mode == "fail-mode"){
        $("#tipsBox").addClass("callout-warning");
    }

    $("#tipsBox #tipsTitle").text(title);
    $("#tipsBox #content").text(content);

    $("#tipsBox").show( "fast", function() {

        setTimeout(function() {
            // Do something after 5 seconds
            $("#tipsBox").hide("slow");
        }, TIPS_BOX_SHOWN_TIME);

    });
}

App.showLoading = function(){
    console.log("showLoading!");
    $("#loadContainer").spin({color: '#FFF'});
    $("#loadContainer").css({'background-color': 'rgba(0,0,0,.3)'});
    $("#loadContainer").show();
}

App.hideLoading = function(){
    console.log("hideLoading!");
    $("#loadContainer").spin(false);
    $("#loadContainer").css({'background-color': 'rgba(0,0,0,.0)'});
    $("#loadContainer").hide();
}

App.startCheckSession = function(){
    console.log("yeah");

    var loopFunc = function(){

        console.log("checkIfLogin fire");

        $.ajax({
            url : "api/checkIfLogin.php",
            method : "POST",
            dataType: "json",
            data : {ajaxLoop:true}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(json){
            console.log(json);

            if(json.status == 0) {
                location.reload();
            }

        }).error(function(d){
            console.log('error');
            console.log(d);
        });

        setTimeout(function(){
            loopFunc();
        },5000);
    };


    setTimeout(function(){
        loopFunc();
    },5000);
}

App.popupBox = App.popupBox || {};



App.popupBox.setupHandler = function(){
    console.log("App.popupBox setupHandler fire!");



    $("#closeBtn").on('click',function(){
        $(".popup_box_container").hide();
    });

    $("#createBtn").on('click',function(){

        $.ajax({
            url : "api/createNewItem.php",
            method : "POST",
            dataType: "text",
            data : {title: $('#title').val(), type: $('#selectionBox :selected').text() }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(json){
            console.log(json);
            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }

            $(".popup_box_container").hide();

            App.libList.destroy();
            App.libList.initialize();

        }).error(function(d){
            console.log('error')
            console.log(d)
        });
    });
}

function searchStringInArr(text, arr){
    var indexArray = new Array();

    $.each( arr, function( index, obj ){
        console.log("obj.key = " + obj.key);

        if(obj.key.toLowerCase().match(text.toLowerCase())){
            indexArray.push(obj);
        }
    });

    console.log("IndexArray = " + indexArray);
    return indexArray;
}

function searchStringInSubject(text, arr){
    var indexArray = new Array();

    $.each( arr, function( index, obj ){
        console.log("obj.firstName = " + obj.subject);
        console.log("text = " + text);

        if(obj.subject.toLowerCase().match(text.toLowerCase())){
            indexArray.push(obj);
        }
    });

    console.log("IndexArray = " + indexArray);
    return indexArray;
}

function searchStringInArrByName(text, arr){
    var indexArray = new Array();

    $.each( arr, function( index, obj ){
        console.log("obj.guestname = " + obj.guestname);
        //console.log("obj.lastName = " + obj.lastName);
        console.log("text = " + text);

        if(obj.guestname.toLowerCase().match(text.toLowerCase())){
            indexArray.push(obj);
        }
    });

    console.log("IndexArray = " + indexArray);
    return indexArray;
}

function searchStringInArrByNameForAviation(text, arr){
    var indexArray = new Array();

    $.each( arr, function( index, obj ){
        console.log("obj.firstName = " + obj.name);
        console.log("text = " + text);

        if(obj.name.toLowerCase().match(text.toLowerCase())){
            indexArray.push(obj);
        }
    });

    console.log("IndexArray = " + indexArray);
    return indexArray;
}

function upperFirstChar(text){

    return text.substr(0,1).toUpperCase()+text.substr(1);
}