App.ItemDetail = Backbone.View.extend({

    titleLangModel: {},
    desLangModel: {},
    currentLang: "en",
    imageObjArray : [],
    preferImageIndex : -1,
    currentItemType : "",
    objDes:{},
    optionModel:[],
    selectOptionModel:[],
    path_length:0,
    path:["fasdf","fasdf"],
    excluded_list: ["Airport", "FX Currency"], //for these item. we don't generate itemList for them
    previewType: "generalItem", //generalItem or inRoomDinningSubcat pr inRoomDinningFood
    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    initialize: function(options){

        if(options && options.paths){

            this.path = options.paths;
        }

        this.render();
    },
    events: {
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){

        var self = this;
        self.optionModel =[];
        self.selectOptionModel = [];

        setTimeout(function() {
            self.setupUI();

        }, 500);



        $.ajax({
            url : "php/html/itemDetail.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){

            console.log(html);
            $(self.el).append(html).
                promise()
                .done(function(){

                    CKEDITOR.replace( 'editor1' );

                    CKEDITOR.replace('editor2', {
                        on: {
                            focus: function(){console.log("focus editor2")},
                            blur: function(){self.saveDescriptionLang()}
                        }
                    });


                    $.ajax({
                        url : "api/getItemList.php",
                        method : "POST",
                        dataType: "json",
                        data : {itemId:App.currentId}
                    }).success(function(json){
                        console.log("firstItemList = " + json);
                        App.currentItem = json.data[0];
                        //alert("type = " + App.currentItem.type);

                        var listTitle = "Sections";
                        var currentItemType = "Library";

                        //if type contains "information", for "with sub menu" and "with top sub menu"
                        if(App.currentItem.type.indexOf("Information") > -1){
                            listTitle = "Sections";
                            self.currentItemType = "Library";
                        }
                        else if(App.currentItem.type.indexOf("Guest request") > -1){
                            listTitle = "Sections";
                            self.currentItemType = "Guest request";
                        }
                        else if(App.currentItem.type.indexOf("Spa/Restaurant") > -1){
                            listTitle = "Sections";
                            self.currentItemType = "Spa/Restaurant";
                        }
                        else if(App.currentItem.type.indexOf("section") > -1){
                            //alert("yeah 1");
                            listTitle = "Articles";
                            self.currentItemType = "Section";

                            self.objDes = App.subHead.getParentType();
                            //alert(self.objDes);
                            if(self.objDes.type == "Guest request"){
                                listTitle = "Services";
                            }
                        }
                        else if(App.currentItem.type.indexOf("article") > -1){
                            listTitle = "Items";
                            self.currentItemType = "Article";
                        }
                        else if(App.currentItem.type.indexOf("sub_item") > -1){
                            listTitle = "Options";
                            self.currentItemType = "Sub Item";
                        }
                        else if(App.currentItem.type.indexOf("item") > -1){
                            listTitle = "Sub Items";
                            self.currentItemType = "Item";

                            //show the price Section for items (items currently only appear in In Room Dinning 1/26/2015
                            $("#priceSection").show();
                        }
                        else if(App.currentItem.type.indexOf("OptionSets") > -1){
                            listTitle = "Option set";
                            self.currentItemType = "OptionSets";
                            //alert("yeah");
                        }
                        else if(App.currentItem.type.indexOf("optionset") > -1){
                            listTitle = "Choice";
                            self.currentItemType = "option set";
                            //alert("yeah");
                        }

                        else if(App.currentItem.type.indexOf("option") > -1){
                            listTitle = "Dependences";
                            self.currentItemType = "Option";
                        }
                        else if(App.currentItem.type.indexOf("dependence") > -1){
                            self.currentItemType = "Dependence";
                        }
                        if(App.currentItem.type == "Information"){
                            listTitle = "Articles";
                        }
                        if(App.currentItem.type == "Earth TV"){
                            listTitle = "Articles";
                        }
                       /* if(App.currentItem.type == "Guest request"){
                            //alert("yeah");
                            listTitle = "Services";
                        }*/
                        if(App.currentItem.type == "service"){
                            self.currentItemType = "Service";

                            //show the command Section for items
                            //$("#commandSection").show();
                        }
                        //$("#commandSection").show();

                        if(App.currentItem.type != "article" && App.currentItem.type != "item"){
                            $("#preview_btn").hide();
                            //$("#ImageContainer, .sub_section").hide();
                        }
                        if(App.currentItem.type != "article"){
                            $("#print_preview_btn").hide();
                            //$("#ImageContainer, .sub_section").hide();
                        }


                        $("#container_timeAvail").hide();

                        self.setupUIHandler();

                        //if the curernt page's item TYPE is Article, we wont have itemList on below
                        if((App.currentItem.type.indexOf("article") > -1 && !App.haveItem) || (App.currentItem.type.indexOf("sub_item") > -1) || App.currentItem.type.indexOf("Web") > -1){

                            if(App.currentItem.type.indexOf("Web") > -1){
                                $("#commandText").text("URL");
                            }
                        }
                        else{ //this is for the case of In-Room Dinning, that ARTICLES has childs of ITEM, so there will be still a ItemList generated.
                            $("#print_preview_btn").hide();

                            if(App.currentItem.type.indexOf("article") > -1) {
                                self.previewType = "inRoomDinningSubcat";
                            }

                            //$("#textEditor_en").hide();
                            //$("#textEditor_lang").hide();
                            if (App.currentItem.type.indexOf("service") <= -1) {

                                if(self.excluded_list.indexOf(App.currentItem.title_en) == -1) {
                                    App.libList = new App.ItemList({parentId: App.currentId, listTitle: listTitle});
                                }

                                if(App.currentItem.type.indexOf("top sub menu") > -1){
                                    //alert("you are in");
                                   /* App.libList = new App.ItemList({parentId: App.currentId, listTitle: listTitle,type:"optionSets"});*/
                                }
                            }
                        }


                        if(App.currentItem.type.indexOf("item") > -1){
                            self.previewType = "inRoomDinningFood";
                        }

                        //get language info for item Title
                        $.ajax({
                            url : "api/getLangForKey.php",
                            method : "POST",
                            dataType: "json",
                            data : {id:App.currentItem.titleId}
                        }).success(function(json){

                            console.log(json.data[0]);

                            self.titleLangModel = json.data[0];

                            App.hideLoading();

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                            App.hideLoading();
                        });

                        //get language info for item Description
                        $.ajax({
                            url : "api/getLangForKey.php",
                            method : "POST",
                            dataType: "json",
                            data : {id:App.currentItem.descriptionId }
                        }).success(function(json){
                            console.log(json.data[0]);

                            self.desLangModel = json.data[0];

                            //$( 'textarea#editor' ).ckeditor();
                            console.log("ding = " + self.desLangModel.zh_hk);


                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });

                        //get photo Info for item
                        self.getPhoto();

                    }).error(function(d){
                        console.log('error');
                        console.log(d);
                    });

                    setTimeout(function(){
                        var editor2 = CKEDITOR.instances.editor2;
                        editor2.setReadOnly(true);
                    },1000);



                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


        //var that = this;

    },
    getPhoto: function(){
        console.log("getPhoto fire");
        var self = this;
        $("#ImageContainer").find(".imageRoot").remove();
        $("#IconContainer").find(".imageRoot").remove();

        $.ajax({
            url : "api/getPhoto.php",
            method : "POST",
            dataType: "json",
            data : {itemId: App.currentId}
        }).success(function(json){

            console.log("json = " + json.data);
            var imageArray = json.data;

            self.imageObjArray = imageArray;

            for(var x = 0;x<imageArray.length;x++){
                console.log(imageArray[x].image);

                var extension =imageArray[x].fileExt=="j"?"jpg":"png";
                var image = new Image();
                image.order =  x;
                image.src = "upload/"+imageArray[x].image+"_s." + extension;
                $(image).addClass("image");
                $(image).attr("title", "Click to assign as prefered image");


                //if the photo is preferred photo, highlight it
                if(imageArray[x].prefer == 1){
                    $(image).addClass("prefer");
                    self.preferImageIndex = x;
                }

                var containerName;
                if(imageArray[x].isIcon == 0){
                    containerName = "#ImageContainer";
                }
                else{
                    containerName = "#IconContainer";
                }

                $(containerName).append("<div id=" + "image"+x + "></div>");
                $("#image"+x).addClass("imageRoot");

                $("#image"+x).attr("order",x);

                $("#image"+x).css({"position":"relative","float":"left"});

                $("#image"+x).append($(image));


                $("#image"+x).append("<a class='removeImage' order=" + x + " ></a>");

                $("#image"+x).on("mouseover", function(){
                    $(this).find(".removeImage").show();
                });

                $("#image"+x).on("mouseout", function(){
                    $(this).find(".removeImage").hide();
                });
                $("#image"+x).on("click", function(){
                    $(".imageRoot").attr("prefer",0);
                    $(".image").removeClass("prefer");

                    $(this).attr("prefer",1);
                    $(this).find(".image").addClass("prefer");

                    self.preferImageIndex = $(this).attr('order');
                });

            };


            //handle remove Image when click on remove button
            $(".removeImage").on("click",function(){
                console.log("delete position " + $(this).attr('order'));

                var order = $(this).attr('order');
                App.yesNoPopup = new App.YesNoPopup(
                    {
                        yesFunc:function()
                        {

                            $.ajax({
                                url : "api/removePhotoForItem.php",
                                method : "POST",
                                dataType: "text",
                                data : {itemId:App.currentId, mediaId: imageArray[order].id}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                            }).success(function(json){

                                console.log(json);
                                if(json.status == 502){
                                    alert(App.strings.sessionTimeOut);
                                    location.reload();
                                    return;
                                }

                                //self.yesFunc();
                                App.yesNoPopup.destroy();
                                self.getPhoto();

                            }).error(function(d){
                                console.log('error');
                                console.log(d);
                            });
                        },
                        msg:"Are you sure to unattach this photo?"
                    }
                );
            });



        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    saveDescriptionLang: function(){
        var editor_lang = CKEDITOR.instances.editor2;

        eval("this.desLangModel."+$("#lang_selectionBox").val()+ "= editor_lang.getData()");
    },
    setupUI: function(){

        objDes = App.subHead.getParentType();

        console.log( "objDes: " + objDes );

        var parentType = objDes.type;
        var isChild = objDes.isChild;
        if(parentType == "Earth TV"){
            $("#commandText").text("Location Code");
        }


        var configObj;
        //App.currentItem.type;

        console.log("parentType = " + parentType);
        console.log( "isChild: " + isChild );

        $.getJSON( "js/item_config.json", function( json ) {
            console.log( "JSON Data: " + json );
        });

        $.ajax({
            url : "js/item_config.json",
            method : "GET",
            dataType: "json",
            data : {}
        }).success(function(json){
            //alert(json);
            console.log("ItemLight 222= " + json);
            console.log(json[parentType]);


            configObj = json[parentType].childs[App.currentItem.type];

            if(!isChild) {
                configObj = json[parentType];
            }

            App.configObj = configObj;

            console.log("jsonObj = " + json[parentType].childs);
            console.log("jsonObj2 = " + json[parentType].childs["section"]);
            console.log("jsonObj = " + json[parentType].childs[App.currentItem.type]);


            console.log("configObj = " + JSON.stringify(configObj));
            console.log("configObj pic = " + configObj.pic);

            configObj.pic == 1? $(".detail_container .sub_section").show(): $("#ImageContainer, .sub_section").hide();
            configObj.icon == 1? $("#iconSection").show(): $("#IconContainer, #iconSection").hide();
            configObj.description == 1? console.log("has description"): $("#textEditor_en, #textEditor_lang").hide();
            configObj.command == 1?  $("#commandSection").show(): $("#commandSection").hide();

            if(parentType == "Earth TV"){
                $("#commandText").text("Location Code");
            }

            console.log("configObj print = " + configObj.print);

            if(configObj.print==null){
                $("#printSection").hide();
                console.log("hide it");
            }
            configObj.print == 1? $("#printSection").show(): $("#printSection").hide();


            if(configObj.preview==null){
                $("#preview_btn").hide();
            }

            if(configObj.preview == 1){
                $("#preview_btn").show();
            }
            else{
                $("#preview_btn").hide();
            }

            if(configObj.minChoice==null){
                $("#container_min").hide();
            }
            if(configObj.maxChoice==null){
                $("#container_max").hide();
            }
            if(configObj.maxQuantity==null){
                $("#container_max_quantity").hide();
            }
            if(configObj.startTime==null){
                $("#container_start_time").hide();
            }
            if(configObj.endTime==null){
                $("#container_end_time").hide();
            }
            if(configObj.optionSets==null){
                $("#optionsContainer").hide();
            }
            else{
                $("#optionSetsText").show();
            }
            if(configObj.noChild!=null){
                $("#item_list_boxer").hide();
            }

            if(configObj.price!=null && configObj.price==1){
                $("#priceSection").show();
            }

            if(configObj.Price!=null && configObj.Price==1){
                $("#priceSection").show();
            }


            $("#shortDesSection").hide();
        }).error(function(d){

            console.log('error');
            console.log(d);
        });

    },
    setupUIHandler: function(){

        var self = this;

        //create the options for the selector
        for(var i = 0; i<21;i++) {
            $('#min_selector').append($('<option>', {
                value: i,
                text: i
            }));
            $('#max_selector').append($('<option>', {
                value: i,
                text: i
            }));
        }

        for(var i = 1; i<11;i++) {
            $('#max_quantity_selector').append($('<option>', {
                value: i,
                text: i
            }));
        }

        //pre-fill the form
        $('#itemName').text(App.currentItem.title_en);
        $('#container_en input').val(App.currentItem.title_en);
        $('#container_lang input').val(App.currentItem.title_en);
        $('#price').val(App.currentItem.price);
        $('#command').val(App.currentItem.command);
        $('#container_timeAvail input').val(App.currentItem.availTime);


        $('#min_selector').val(App.currentItem.minChoice);
        $('#max_selector').val(App.currentItem.maxChoice);
        $('#max_quantity_selector').val(App.currentItem.maxQuantity);


        $('#start_time').combodate({
            firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
            minuteStep: 30
        });

        $('#end_time').combodate({
            firstItem: 'name', //show 'hour' and 'minute' string at first item of dropdown
            minuteStep: 30
        });

        $('#start_time').combodate('setValue', App.currentItem.startTime.substr(0,5));
        $('#end_time').combodate('setValue', App.currentItem.endTime.substr(0,5));


        App.currentItem.print == 1?$("#printCheckBox").prop('checked', true):$("#printCheckBox").prop('checked', false);


        var editor_en = CKEDITOR.instances.editor1;
        var editor_lang = CKEDITOR.instances.editor2;

        //alert(App.currentItem.description_en);
        //alert(editor_en);

        setTimeout(function(){
        editor_en.setData(App.currentItem.description_en);
        },500);

        //set the Save and delete button text
        $("#save_btn").text("Save " + self.currentItemType);
        $("#delete_btn").text("Delete " + self.currentItemType);

        //handle delete button
        $('#delete_btn').on('click',function(){
            //alert("delete item id = " + App.currentId);
            App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc:function()
                    {
                        $.ajax({
                            url : "api/deleteItem.php",
                            method : "POST",
                            dataType: "text",
                            data : {id:App.currentId }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                        }).success(function(json){

                            console.log(json);
                            //self.yesFunc();
                            App.yesNoPopup.destroy();

                            App.goUpperLevel();

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    },
                    msg:"Are you sure to delete this item?"
                }
            );
        });

        //handle save button
        $('#save_btn').on('click',function(){
            //alert("that = "+ that);
            //save the description for other language first
            self.saveDescriptionLang();

            var showPrint = 0;
            if($("#printCheckBox").is(':checked'))
                showPrint = 1;  // checked

            //update Title of Item
            $.ajax({
                url : "api/updateItemDict.php",
                method : "POST",
                dataType: "text",
                data : {title_id:App.currentItem.titleId,
                    title_en: $('#detailTitle_en').val(),
                    title_zh_hk: self.titleLangModel.zh_hk,
                    title_zh_cn: self.titleLangModel.zh_cn,
                    title_jp: self.titleLangModel.jp,
                    title_fr: self.titleLangModel.fr,
                    title_ar: self.titleLangModel.ar,
                    title_es: self.titleLangModel.es,
                    title_de: self.titleLangModel.de,
                    title_ko: self.titleLangModel.ko,
                    title_ru: self.titleLangModel.ru,
                    title_pt: self.titleLangModel.pt,
                    title_tr: self.titleLangModel.tr

                }
            }).success(function(json){
                //alert("yeah");
                console.log(json);
                console.log("detailTitle_en = " +  $('#detailTitle_en').val());
                //self.yesFunc();
                //self.destroy();

                if(json.status == 502){
                    alert(App.strings.sessionTimeOut);
                    location.reload();
                    return;
                }


                //update Description of Item
                $.ajax({
                    url : "api/updateItemDict.php",
                    method : "POST",
                    dataType: "text",
                    data : {title_id:App.currentItem.descriptionId,
                        title_en: editor_en.getData(),
                        title_zh_hk: self.desLangModel.zh_hk,
                        title_zh_cn: self.desLangModel.zh_cn,
                        title_jp: self.desLangModel.jp,
                        title_fr: self.desLangModel.fr,
                        title_ar: self.desLangModel.ar,
                        title_es: self.desLangModel.es,
                        title_de: self.desLangModel.de,
                        title_ko: self.desLangModel.ko,
                        title_ru: self.desLangModel.ru,
                        title_pt: self.desLangModel.pt,
                        title_tr: self.desLangModel.tr
                    }
                }).success(function(json){
                    //alert("yeah");
                    console.log(json);
                    if(json.status == 502){
                        alert(App.strings.sessionTimeOut);
                        location.reload();
                        return;
                    }

                    console.log("detailDes_en = " +  $('#detailDes_en').val());
                    //self.yesFunc();
                    //self.destroy();
                    //console.log("img array = " + that.imageObjArray);
                    //console.log(that.imageObjArray[that.preferImageIndex]);

                    if(self.preferImageIndex != -1){
                        //update prefer Image
                        $.ajax({
                            url : "api/setPreferImage.php",
                            method : "POST",
                            dataType: "text",
                            data : {itemId:App.currentId,mediaId:self.imageObjArray[self.preferImageIndex].id
                            }
                        }).success(function(json){
                            //alert("yeah");
                            console.log("setPreferImage = " + json);

                            //self.yesFunc();
                            //self.destroy();

                            if(true||self.currentItemType == "Item" || self.currentItemType == "Service"){
                                $.ajax({
                                    url : "api/updateItem.php",
                                    method : "POST",
                                    dataType: "json",
                                    data : {price:$("#price").val(),
                                            item_id:App.currentId,
                                            commmand:$("#command").val(),
                                            timeAvail:$('#container_timeAvail input').val(),
                                            print:showPrint,
                                            maxChoice: $('#max_selector').val(),
                                            minChoice: $('#min_selector').val(),
                                            maxQuantity: $('#max_quantity_selector').val(),
                                            startTime: $('#start_time').combodate('getValue', 'HH:mm'),
                                            endTime: $('#end_time').combodate('getValue', 'HH:mm'),
                                            optionSetIds: self.genListOfOptionSets(self)
                                    }
                                }).success(function(json){
                                    //alert("yeah");
                                    console.log("updateItem result = " + json);
                                    if(json.status == 502){
                                        alert(App.strings.sessionTimeOut);
                                        location.reload();
                                        return;
                                    }

                                    if(json.status == '1'){
                                        App.goUpperLevel();
                                    }
                                    else if (json.status == '0'){
                                        alert(json.msg);
                                    }

                                }).error(function(d){
                                    console.log('error');
                                    console.log(d);
                                });
                            }
                            else{
                                App.goUpperLevel();
                            }

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    }
                    else{

                        if(self.currentItemType == "Item" || self.currentItemType == "Service" || true){
                            $.ajax({
                                url : "api/updateItem.php",
                                method : "POST",
                                dataType: "json",
                                data : {price:$("#price").val(),
                                        item_id:App.currentId,
                                        command:$("#command").val(),
                                        shortDes:$("#shortDes").val(),
                                        print:showPrint,
                                        maxChoice: $('#max_selector').val(),
                                        minChoice: $('#min_selector').val(),
                                        maxQuantity: $('#max_quantity_selector').val(),
                                        startTime: $('#start_time').combodate('getValue', 'HH:mm'),
                                        endTime: $('#end_time').combodate('getValue', 'HH:mm'),
                                        optionSetIds: self.genListOfOptionSets(self)
                                }
                            }).success(function(json){
                                //alert("yeah");
                                //alert("yeah");
                                console.log("updateItem result = " + json);
                                console.log(json['status']);
                                console.log(json['msg']);

                                if(json.status == 502){
                                    alert(App.strings.sessionTimeOut);
                                    location.reload();
                                    return;
                                }

                                if(json.status == 1){
                                    App.goUpperLevel();
                                }
                                else if (json.status == 0){
                                    alert(json.msg);
                                }

                            }).error(function(d){
                                console.log('error');
                                console.log(d);
                            });
                        }
                        else{
                            App.goUpperLevel();
                        }
                    }



                }).error(function(d){
                    console.log('error');
                    console.log(d);
                });

            }).error(function(d){
                console.log('error');
                console.log(d);
            });
            /*App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc:function()
                    {
                        alert("yes");
                    }
                }
            );*/
        });

        //handle language select box
        $("#lang_selectionBox").change(function() {

            var editor_lang = CKEDITOR.instances.editor2;

            if( $("#lang_selectionBox").val() != ""){
                self.currentLang = $("#lang_selectionBox").val();
            }

            //setup enable/disable the other language input box
            if($("#lang_selectionBox").val() == 'en'){
                //alert("disable");
                $('#container_lang input').attr('disabled','disabled');
                editor_lang.setReadOnly(true);
            }
            else{
                //alert("enable");
                $('#container_lang input').removeAttr('disabled');
                editor_lang.setReadOnly(false);
            }

            $('#container_lang input').val(eval("self.titleLangModel."+$("#lang_selectionBox").val()));

            editor_lang.setData(eval("self.desLangModel."+$("#lang_selectionBox").val()));
        });

        $('#container_lang input').blur(function() {
            var editor_lang = CKEDITOR.instances.editor2;
            //alert("hi");
            eval("self.titleLangModel."+$("#lang_selectionBox").val()+ "= $('#container_lang input').val()");

            console.log(self.titleLangModel);
        });

        $("#addImageBtn").on('click',function(){
            App.addPhotoPopup = new App.AddPhotoPopup(
                {
                    root:self
                }
            );
        });

        $("#addIconBtn").on('click',function(){
            App.addPhotoPopup = new App.AddPhotoPopup(
                {
                    root:self,
                    type: "icon"
                }
            );
        });

        $("#preview_btn").on('click',function(){
            self.saveDescriptionLang();

            var currentLang = $("#lang_selectionBox").val(),
                toSendTitle = $('#detailTitle_en').val(),
                toSendDes = editor_en.getData();

            if (currentLang != "en"){
                toSendTitle = eval("self.titleLangModel."+$("#lang_selectionBox").val());
                toSendDes = eval("self.desLangModel."+$("#lang_selectionBox").val());
            }

            App.previewPopup = new App.PreviewPopup(
                {
                    root:self,
                    previewType: self.previewType,
                    itemTitle: toSendTitle,
                    parentTitle: App.subHead.getParentText(),
                    description: toSendDes,
                    lang: currentLang,
                    price: $("#price").val(),
                    itemImageLink: self.preferImageIndex!= -1?"upload/"+self.imageObjArray[self.preferImageIndex].image+"_m.jpg":null
                }
            );
        });

        $("#print_preview_btn").on('click',function(){

            var currentLang = $("#lang_selectionBox").val(),
                toSendTitle = $('#detailTitle_en').val(),
                toSendDes = editor_en.getData();

            if (currentLang != "en"){
                toSendTitle = eval("self.titleLangModel."+$("#lang_selectionBox").val());
                toSendDes = eval("self.desLangModel."+$("#lang_selectionBox").val());
            }

            App.previewPopupTabPrint = new App.PreviewPopupTabPrint(
                {
                    root:self,
                    previewType: "CMS_item",
                    itemTitle: toSendTitle,
                    parentTitle: App.subHead.getParentText(),
                    description: toSendDes,
                    price: $("#price").val(),
                    itemImageLink: self.preferImageIndex!= -1?"upload/"+self.imageObjArray[self.preferImageIndex].image+"_m.jpg":null
                }
            );
        });

        if(App.currentItem.type=="item" || App.currentItem.type=="sub_item") {

            $.ajax({
                url : "api/ird/getOptionSets.php",
                method : "GET",
                dataType: "json",
                data : {}
            }).success(function(json){
                self.optionModel = json.data;


                self.postHandleOptionModel(self);
                self.initOptionList(self);
                self.initSelectedOptionList(self);



            }).error(function(d){

                console.log('error');
                console.log(d);
            });
        }


        $(".image").tooltip();
    },
    postHandleOptionModel: function(_self){
        var self = _self;
        var optionLength  = self.optionModel.length;

        console.log("optionModel length = " + self.optionModel.length);
        var idArray = new Array();

        idArray = App.currentItem.optionSetIds.split(",");
        console.log("idArray = " + idArray);

        var tempArray = new Array();
        for(var x = 0; x < optionLength; x++){
            console.log("optionLength = " + self.optionModel[x]);
            for(var y = 0; y < idArray.length;y++){

                if (self.optionModel[x]!= null && self.optionModel[x].id == idArray[y]){
                    var exist = false;
                    for(var z = 0;z<self.selectOptionModel.length;z++){
                        if(self.selectOptionModel[z].id == idArray[y]){
                            exist = true;
                        }
                    }

                    if(!exist) {
                        self.selectOptionModel.push(self.optionModel[x]);


                    }

                    self.optionModel.splice(x, 1);
                    x--;

                }
            }
        }

        //alert("option Array 's length = " + self.optionModel.length);
        console.log("temp array = " + tempArray);

    },

    initOptionList: function(_self){
        console.log("initOptionList fire");
        var self = _self;
        var optionLength  = self.optionModel.length;
        /*console.log("self.guestModel = "+self.guestModel );
        console.log("self.select_guestModel = "+self.selectGuestModel );*/
        console.log("optionLength = "+optionLength );

        //clear up first
        $("#option_fixList").empty();

        for(var x = 0; x < optionLength; x++){

            var itemRoot =document.createElement('div');
            $(itemRoot).addClass("option_item_container");

            $("#option_fixList").append($(itemRoot));
            console.log("y = " + x);
            $(itemRoot).append("<div class='first_column' style='width:75%'>"+ self.optionModel[x].en +"</div>");

            //$(itemRoot).append("<div class='second_column'>" +self.guestModel[x].guestname +"</div>");
            $(itemRoot).append("<div class='add_button'" + "order ="+ x +" id="+"add"+x+">+</div>");

            $("#add"+x).on('click',function(){
                var index = $(this).attr("order");


                self.selectOptionModel.push(self.optionModel[index]);

                self.optionModel.splice(index,1);

                self.initOptionList(self);
                self.initSelectedOptionList(self);
            });
        }
    },
    initSelectedOptionList: function(_self){
        console.log("init Selected OptionList fire");
        var self = _self;
        var optionLength  = self.selectOptionModel.length;
        /*console.log("self.guestModel = "+self.guestModel );
        console.log("self.select_guestModel = "+self.selectGuestModel );*/
        console.log("selected optionLength = "+optionLength );

        self.selectOptionModel.sort(function(a,b){
           return a.order - b.order;
        });
        //clear up first
        $("#selected_option_fixList").empty();

        for(var x = 0; x < optionLength; x++){

            var itemRoot =document.createElement('div');
            $(itemRoot).addClass("option_item_container");
            $(itemRoot).attr("id",self.selectOptionModel[x].id);

            $("#selected_option_fixList").append($(itemRoot));
            console.log("x = " + x);
            $(itemRoot).append("<div class='first_column' style='width:75%'>"+ self.selectOptionModel[x].en +"</div>");

            $(itemRoot).append("<div class='add_button pink'" + "order ="+ x +" id="+"delete"+x+">X</div>");
            $(itemRoot).append("<div class='swapBtn' id = " + "swap"+ x +" title='drag it to swap the order.'></div>");

            $("#delete"+x).on('click',function(){
                var index = $(this).attr("order");


                self.optionModel.push(self.selectOptionModel[index]);

                self.selectOptionModel.splice(index,1);

                self.initOptionList(self);
                self.initSelectedOptionList(self);
            });
        }

        /*setup swapping function*/
        var fixHelper = function (e, tr) {
            var $originals = tr.children();
            var $helper = tr.clone();
            $helper.children().each(function (index) {
                // Set helper cell sizes to match the original sizes
                $(this).width($originals.eq(index).width());
            });

            // append it to the body to avoid offset dragging
            $("#selected_option_fixList").append($helper);

            return $helper;
        }

        var stopHelper = function(e, ui) {
            //alert("stop sorting");
            //alert( "Index: " + $("tr").index( $( "#140" )));
            console.log("total = " + App.itemDetail.selectOptionModel.length);

            var idArr = new Array();
            var orderArr = new Array();


            $.each( App.itemDetail.selectOptionModel, function( index, obj ){
                console.log("obj.id = " + obj.id);

                obj.order = $("#selected_option_fixList .option_item_container ").index($( "#"+obj.id));

                console.log("obj.order = " + obj.order);
                console.log("obj= " + obj);
                idArr.push(obj.id);
                orderArr.push(obj.order-1);
            });


            App.showLoading();

            $.ajax({
                url : "api/saveOrderForItem.php",
                method : "POST",
                dataType: "json",
                data : {idArr:idArr.join(), orderArr:JSON.stringify(orderArr)}
            }).success(function(json){
                console.log(json);
                if(json.status == 502){
                    alert(App.strings.sessionTimeOut);
                    location.reload();
                    return;
                }

                App.hideLoading();

                if(json.status == 1){
                    console.log("The order is saved.");
                    App.showTipBox("suscess-mode","Suscess","The order is saved.");
                }

            }).error(function(d){
                console.log('error');
                console.log(d);
                App.hideLoading();
                App.showTipBox("fail-mode","Failed","Please try again later");

            });

            return ui;
        }


        $('#selected_option_fixList').sortable({
            helper: fixHelper,
            axis: 'y',
            stop: stopHelper
        }).disableSelection();

        /*$('.headRow').sortable({
         helper: fixHelper,
         axis: 'y'
         }).disableSelection();*/

        // $('.headRow').sortable( "disable" );
        $('#selected_option_fixList').sortable( "disable" );

        $('.swapBtn').on('mouseover',function(){
            $('#selected_option_fixList').sortable( "enable" );
        });
        $('.swapBtn').on('mouseout',function(){
            $('#selected_option_fixList').sortable( "disable" );
        });
    },
    genListOfOptionSets: function(_self){
        var self = _self;
        var ret = "";
        for(var x = 0;x<self.selectOptionModel.length;x++){
            if(x<self.selectOptionModel.length-1) {
                ret += self.selectOptionModel[x].id+",";
            }
            else{
                ret += self.selectOptionModel[x].id;
            }
        }
        return ret;
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();

        //this.$el.removeData().unbind();

        //Remove view from DOM
        $(this.el).empty();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});