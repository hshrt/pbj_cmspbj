App.KeysView = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    keyObjects:null,
    keyObjectsFiltered:null,
    page:0,
    itemPerPage:15,
    filtering:false,
    initialize: function(options){
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
    },

    render: function(){
        //alert($(window).width());
        var self = this;
        $.ajax({
            url : "php/html/keysView.php",
            method : "POST",
            dataType: "html",
            data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(html){

            $(self.el).append(html).
                promise()
                .done(function(){

                });

            $("#addBtn").on("click", function(){
                App.addKeyPopup = new App.AddKeyPopup(
                    {
                        root:self
                    }
                );
            });

            $.ajax({
                url : "api/getLangForKey.php",
                method : "POST",
                dataType: "json",
                data : {getCount: true}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
            }).success(function(json){
                console.log("json = " + json);
                console.log("jsondasdasdasdadasdasdadasd = " + json.data[0].totalKeysNum);
                console.log("json = " + json.data);

                self.updateNavigation(json.data[0].totalKeysNum);

                $("#search").change(function(){

                });
                $('#search').on('keyup', function() {
                    if (this.value.length > 0) {
                        self.keyObjectsFiltered = searchStringInArr($("#search").val(),self.keyObjects);

                        $.each(self.keyObjects, function( index, obj){

                                obj.filtered = false;

                        });

                        if(self.keyObjectsFiltered == 0){
                            $("#itemContainer").empty();
                            $("#paginationContainer").hide();
                            $("#noResultMsg").show();

                        }
                        else{
                            console.log("yeah");
                            $("#paginationContainer").show();
                            $("#noResultMsg").hide();

                            self.filtering = true;
                            $.each(self.keyObjects, function( index, obj){

                                if(self.keyObjectsFiltered.indexOf(obj) > -1){
                                    obj.filtered = true;
                                }
                            });

                            self.page = 0;
                            self.loadKeysItem(self.page);
                        }
                    }
                    else{
                        $("#paginationContainer").show();
                        $("#noResultMsg").hide();
                        self.filtering = false;
                        self.page = 0;
                        self.loadKeysItem(self.page);
                    }
                });

                self.loadKeysItem(self.page);

            }).error(function(d){
                console.log('error');
                console.log(d);
            });


        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },
    updateNavigation: function(_items){

        var self = this;

        $("#paginationContainer").pagination({
            items: _items,
            itemsOnPage: self.itemPerPage,
            cssStyle: 'light-theme',
            onPageClick:function(pageNum, event){
                //alert("hi" + pageNum);
                self.page = pageNum -1;
                self.loadKeysItem(pageNum-1);

            }
        });
    },
    goBackToFirstPage: function(){
        $("#paginationContainer").pagination('selectPage', 1);
    },
    refreshKeys: function(){
        //self.page = 0;
        var self = this;
        self.keyObjects = null;
        self.keyObjectsFiltered = null;
        $("#search").val("");
        self.loadKeysItem(self.page);
    },
    loadKeysItem: function(page){

        var self = this;

        $("#itemContainer").empty();

        if(self.keyObjects == null){
            $.ajax({
                url : "api/getLangForKey.php",
                method : "POST",
                dataType: "json",
                data : {page:self.page, getAllKey: true, itemPerPage:self.itemPerPage}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
            }).success(function(json){
                console.log("json gaga= " + json);
                console.log("json gaga = " + json.data.length);
                console.log("json gaga= " + json.data[0]);

                self.keyObjects =  json.data;
                   //alert(self.keyObjects.length);
                $("#paginationContainer").pagination('updateItems', self.keyObjects.length);

                for(var x = page*self.itemPerPage; x < self.itemPerPage*(page+1); x++){

                    var itemRoot =document.createElement('div');
                    $(itemRoot).addClass("itemRoot");

                    $("#itemContainer").append($(itemRoot));

                    $(itemRoot).append('<div id='+ 'keyText'+ x+'></div>');
                    $("#keyText"+ x).text(self.keyObjects[x].key);
                    $("#keyText"+ x).addClass("keyText");

                    $(itemRoot).append('<div id='+ 'global'+ x+' class = '+'button-globe '+ 'order ='+ x+  " title='click to edit translation of keyword'"+'></div>');
                    $(itemRoot).append("<div id="+'input'+x+" class='engText'"+'></div>');

                    $("#input"+x).text(self.keyObjects[x].en);

                    $("#global"+x).on('click',function(){

                        App.addKeyPopup = new App.AddKeyPopup(
                            {
                                root:App.keysView,
                                keyObj:App.keysView.keyObjects[$(this).attr("order")],
                                isEdit:true
                            }
                        );
                    });
                }

                //enable the show tip box function for swap button (a request by Chris)
                $( ".button-globe" ).tooltip();



            }).error(function(d){
                console.log('error');
                console.log(d);
            });
        }
        else{

            var keyObj = self.filtering?self.keyObjectsFiltered:self.keyObjects;

            var upperLimit = keyObj.length<self.itemPerPage?keyObj.length:self.itemPerPage;

            $("#paginationContainer").pagination('updateItems', keyObj.length);

            for(var x = page*self.itemPerPage ;x < upperLimit*(page+1); x++){




                //if(!self.filtering || self.keyObjects[x].filtered){
                    var itemRoot =document.createElement('div');
                    $(itemRoot).addClass("itemRoot");

                    $("#itemContainer").append($(itemRoot));

                    $(itemRoot).append('<div id='+ 'keyText'+ x+'></div>');
                    $("#keyText"+ x).text(keyObj[x].key);
                    $("#keyText"+ x).addClass("keyText");

                    $(itemRoot).append('<div id='+ 'global'+ x+' class = '+'button-globe '+ 'order ='+ x+ '></div>');
                    $(itemRoot).append("<div id="+'input'+x+" class='engText'"+'></div>');

                    $("#input"+x).text(keyObj[x].en);

                    $("#global"+x).on('click',function(){

                        App.addKeyPopup = new App.AddKeyPopup(
                            {
                                root:App.keysView,
                                keyObj:keyObj[$(this).attr("order")],
                                isEdit:true
                            }
                        );
                    });
                //}
            }
        }
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});