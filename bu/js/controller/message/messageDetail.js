App.MessageDetail = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.

    titleLangModel: {},
    desLangModel: {},
    guestModel: [],
    roomModel: [],
    selectGuestModel: [],
    currentLang: "en",
    parentId: 0,
    jsonObj: null,
    type:null, //indicate if this message Detail is for making new message or edit existed message
    title: "Guest",
    initialize: function(options){
        this.selectGuestModel = [];
        console.log("self.selectGuestModel legnth = " + this.selectGuestModel.length);
        console.log("self.guestModel legnth = " + this.guestModel.length);
        if(options && options.parentId){
            this.parentId = options.parentId;
        }
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        if(options && options.type){
            this.type = options.type;
        }
        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){

        var self = this;
        //console.log("App.messageDetail legnth = " + App.messageDetail.selectGuestModel.length);
        console.log("self.selectGuestModel legnth = " + self.selectGuestModel.length);

        self.selectGuestModel = [];

        self.type != "new" && setTimeout(function(){App.subHead.setPathText(-1,App.currentItem.subject);}, 1000);

        //remove any cached data if it is for new message
        if(self.type == "new") {
            App.currentItem = [];
            App.currentId = "";
        }

        $("#itemName").text("Compose");

        $.ajax({
            url : "php/html/msg/messageDetail.php",
            method : "POST",
            method : "POST",
            dataType: "html",
            data : { }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(html){
            $(self.el).append(html).
                promise()
                .done(function(){
                    //self.loadPhoto(self.page);

                    CKEDITOR.replace( 'editor1' );

                    CKEDITOR.replace('editor2', {
                        on: {
                            focus: function(){console.log("focus editor2")},
                            blur: function(){self.saveDescriptionLang()}
                        }
                    });

                    //set date picker
                    $( "#startDateInput" ).datepicker({ dateFormat: 'yy-mm-dd' });
                    $( "#endDateInput" ).datepicker({dateFormat: 'yy-mm-dd' });


                    //this is editing existed message
                    if(self.type != "new"){

                        $.ajax({
                            url : "api/msg/getMessage.php",
                            method : "POST",
                            dataType: "json",
                            data : {itemId:App.currentId }
                        }).success(function(json){
                            console.log("firstItemList = " + json);
                            App.currentItem = json.data[0];


                            $("#startDateInput").val(App.currentItem.startDate.substr(0,10));
                            $("#endDateInput").val(App.currentItem.endDate.substr(0,10));

                            self.setupUIHandler();

                            //get language info for item Title
                            $.ajax({
                                url : "api/getLangForKey.php",
                                method : "POST",
                                dataType: "json",
                                data : {id:App.currentItem.subjectId, forMsg:1}
                            }).success(function(json){
                                console.log(json.data[0]);


                                self.titleLangModel = json.data[0];

                                App.hideLoading();

                                //console.log(that.titleLangModel.zh_hk);

                            }).error(function(d){
                                console.log('error');
                                console.log(d);
                                App.hideLoading();
                            });

                            //get language info for item Description
                            $.ajax({
                                url : "api/getLangForKey.php",
                                method : "POST",
                                dataType: "json",
                                data : {id:App.currentItem.descriptionId, forMsg:1 }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                            }).success(function(json){
                                console.log(json.data[0]);

                                self.desLangModel = json.data[0];

                                //$( 'textarea#editor' ).ckeditor();
                                console.log("ding = " + self.desLangModel.zh_hk);


                            }).error(function(d){
                                console.log('error');
                                console.log(d);
                            });

                            //get the guest list
                            $.ajax({
                                url : "api/guest/getAllGuest.php",
                                method : "POST",
                                dataType: "json",
                                data : {forMsg: true, msgId:App.currentId}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                            }).success(function(json){
                                //console.log(json.data[0]);

                                self.guestModel = json.data;

                                //$( 'textarea#editor' ).ckeditor();
                                console.log("guestModel = " + self.guestModel);

                                //this function will filter out those guest who is already have this message, and put the guest to selectGuestModel array

                                self.postHandleGuestModel(self);

                                self.initGuestList(self);
                                self.initSelectGuestList(self);


                            }).error(function(d){
                                console.log('error');
                                console.log(d);
                            });



                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    }
                    else{//this is for new message
                        self.setupUIHandler();

                        //get the checked in guest list
                        $.ajax({
                            url : "api/guest/getAllGuest.php",
                            method : "POST",
                            dataType: "json",
                            data : {forMsg: true, msgId:App.currentId}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                        }).success(function(json){
                            //console.log(json.data[0]);

                            self.guestModel = json.data;

                            //$( 'textarea#editor' ).ckeditor();
                            console.log("guestModel = " + self.guestModel);

                            //this function will filter out those guest who is already have this message, and put the guest to selectGuestModel array

                            self.postHandleGuestModel(self);

                            self.initGuestList(self);
                            self.initSelectGuestList(self);


                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });

                        //get the room list
                        $.ajax({
                            url : "api/guest/getAllGuest.php",
                            method : "POST",
                            dataType: "json",
                            data : {$checkedIn: true}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                        }).success(function(json){
                            //console.log(json.data[0]);

                            self.roomModel = json.data;

                            //$( 'textarea#editor' ).ckeditor();
                            console.log("guestModel = " + self.roomModel);

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    }

                    setTimeout(function(){


                        var editor2 = CKEDITOR.instances.editor2;
                        editor2.setReadOnly(true);
                    },1000);

                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function(){
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library"?"Page":self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    setupUIHandler: function(){
        var self = this;


        //pre-fill the form
        self.type != "new"? $('#itemName').text(App.currentItem.subject):$('#itemName').text("Compose");

        $('#container_en input').val(App.currentItem.subject);
        $('#container_lang input').val(App.currentItem.subject);

        var editor_en = CKEDITOR.instances.editor1;
        var editor_lang = CKEDITOR.instances.editor2;

        setTimeout(function(){
            //alert(self.desLangModel.en);
            var string  = "";
            if(self.desLangModel.en.indexOf("<br />")==-1 && App.currentItem.lastUpdateBy == "TC3"){
                string = self.desLangModel.en.replace(/\n/g, "<br />");
                //string  = string.replace(/\r/g, "<br />");
            }
            else{
                string = self.desLangModel.en;
            }
            //string = string.replace(/\r/g, "<br />");
            editor_en.setData(string);
            //eidtor_en.setDa
        },1000);

        //set the Save and delete button text
        $("#save_btn").text("Save " + "Message");
        self.type != "new"?$("#delete_btn").text("Delete Message"):$("#delete_btn").hide();

        //set the radio button
        if(App.currentItem.boardcast == 1){
            $("#all_guest_radio").prop('checked', true);
            $("#custom_guest_container").hide();
        }
        else if(App.currentItem.boardcast == 0){
            $("#custom_radio").prop('checked', true);
        }
        else if(App.currentItem.boardcast == 2){
            $("#all_guest_popup_radio").prop('checked', true);
            $("#custom_guest_container").hide();
        }

        $('#all_guest_radio').change(function(){

            if($(this).prop("checked") === true){
                $("#custom_guest_container").hide();
                $("#custom_radio").prop('checked', false);
                $("#all_guest_popup_radio").prop('checked', false);
            }
        });

        $('#custom_radio').change(function(){

            if($(this).prop("checked") === true){
                $("#custom_guest_container").show();
                $("#all_guest_radio").prop('checked', false);
                $("#all_guest_popup_radio").prop('checked', false);
            }
        });

        $('#all_guest_popup_radio').change(function(){

            if($(this).prop("checked") === true){
                $("#custom_guest_container").hide();
                $("#all_guest_radio").prop('checked', false);
                $("#custom_radio").prop('checked', false);
            }
        });

        //set the priority click box
        if(App.currentItem.priority == 1){
            $("#priority_checkbox").prop('checked', true);
        }
        else{
            $("#priority_checkbox").prop('checked', false);
        }

        //handle delete button
        $('#delete_btn').on('click',function(){
            //alert("delete item id = " + App.currentId);
            App.yesNoPopup = new App.YesNoPopup(
                {
                    yesFunc:function()
                    {
                        $.ajax({
                            url : "api/msg/markAsInactive.php",
                            method : "POST",
                            dataType: "json",
                            data : {msgId:App.currentId }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                        }).success(function(json){
                            if(json.status == 502){
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }

                            console.log(json);
                            //self.yesFunc();
                            App.yesNoPopup.destroy();

                            App.goUpperLevel();

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    },
                    msg:"Are you sure to mark this message as inactive?"
                }
            );
        });

        //handle save button
        $('#save_btn').on('click',function(){
            console.log("save btn clicked");

            //save the description for other language first
            self.saveDescriptionLang();

            //update exist message
            if(self.type != "new"){
                //update Title of Item
                $.ajax({
                    url : "api/updateItemDict.php",
                    method : "POST",
                    dataType: "text",
                    data : {title_id:App.currentItem.subjectId,
                        title_en: $('#detailTitle_en').val(),
                        title_zh_hk: self.titleLangModel.zh_hk,
                        title_zh_cn: self.titleLangModel.zh_cn,
                        title_jp: self.titleLangModel.jp,
                        title_fr: self.titleLangModel.fr,
                        title_ar: self.titleLangModel.ar,
                        title_es: self.titleLangModel.es,
                        title_de: self.titleLangModel.de,
                        title_ko: self.titleLangModel.ko,
                        title_ru: self.titleLangModel.ru,
                        title_pt: self.titleLangModel.pt,
                        forMsg  : 1

                    }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                }).success(function(json){
                    //alert("yeah");
                    console.log(json);
                    if(json.status == 502){
                        alert(App.strings.sessionTimeOut);
                        location.reload();
                        return;
                    }

                    console.log("detailTitle_en = " +  $('#detailTitle_en').val());
                    //self.yesFunc();
                    //self.destroy();

                    //update Description of Item
                    $.ajax({
                        url : "api/updateItemDict.php",
                        method : "POST",
                        dataType: "text",
                        data : {title_id:App.currentItem.descriptionId,
                            title_en: editor_en.getData(),
                            title_zh_hk: self.desLangModel.zh_hk,
                            title_zh_cn: self.desLangModel.zh_cn,
                            title_jp: self.desLangModel.jp,
                            title_fr: self.desLangModel.fr,
                            title_ar: self.desLangModel.ar,
                            title_es: self.desLangModel.es,
                            title_de: self.desLangModel.de,
                            title_ko: self.desLangModel.ko,
                            title_ru: self.desLangModel.ru,
                            title_pt: self.desLangModel.pt,
                            forMsg: 1
                        }
                    }).success(function(json){
                        //alert("yeah");
                        console.log(json);
                        console.log("detailDes_en = " +  $('#detailDes_en').val());
                        //self.yesFunc();
                        //self.destroy();
                        //console.log("img array = " + that.imageObjArray);
                        //console.log(that.imageObjArray[that.preferImageIndex]);

                        var guestMsgIdlist = "";

                        for(var x = 0; x< self.selectGuestModel.length;x++){
                            if(x !=self.selectGuestModel.length-1){
                                guestMsgIdlist+=self.selectGuestModel[x].room+",";
                            }
                            else{
                                guestMsgIdlist+=self.selectGuestModel[x].room;
                            }
                        }
                        console.log("guestMsgIdlist  = " + guestMsgIdlist);

                        var boardcastVar = $('#all_guest_radio').prop('checked')?1:0;

                        if($('#all_guest_popup_radio').prop('checked')){
                            boardcastVar = 2;
                        }

                        //update Description of Item
                        $.ajax({
                            url : "api/msg/updateMessage.php",
                            method : "POST",
                            dataType: "json",
                            data : {msgId:App.currentId,
                                guestMsgIdList: guestMsgIdlist,
                                startDate:$("#startDateInput").val() + " 00:00:00",
                                endDate:$("#endDateInput").val()+ " 00:00:00",
                                priority:$('#priority_checkbox').prop('checked')?1:0,
                                boardcast:boardcastVar
                            }
                        }).success(function(json){
                            //alert("yeah");
                            console.log(json);
                            App.goUpperLevel();
                            //console.log("detailDes_en = " +  $('#detailDes_en').val());
                            //self.yesFunc();
                            //self.destroy();
                            //console.log("img array = " + that.imageObjArray);
                            //console.log(that.imageObjArray[that.preferImageIndex]);



                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });


                    }).error(function(d){
                        console.log('error');
                        console.log(d);
                    });

                }).error(function(d){
                    console.log('error');
                    console.log(d);
                });
            }
            //add mew message
            else{

                var guestMsgIdlist = "";

                for(var x = 0; x< self.selectGuestModel.length;x++){
                    if(x !=self.selectGuestModel.length-1){
                        guestMsgIdlist+=self.selectGuestModel[x].room+",";
                    }
                    else{
                        guestMsgIdlist+=self.selectGuestModel[x].room;
                    }
                }


                console.log("guestMsgIdlist  = " + guestMsgIdlist);

                if($('#all_guest_radio').prop('checked') == 1){
                    guestMsgIdlist = ""
                }

                var boardcastVar = $('#all_guest_radio').prop('checked')?1:0;

                if($('#all_guest_popup_radio').prop('checked')){
                    boardcastVar = 2;

                    //put all room into the guestMsgIdList, boardcast(popup) message will be sent to all room including those not check in
                    for(var x = 0; x< self.roomModel.length;x++){
                        if(x !=self.roomModel.length-1){
                            guestMsgIdlist+=self.roomModel[x].room+",";
                        }
                        else{
                            guestMsgIdlist+=self.roomModel[x].room;
                        }
                    }
                }


                //update Description of Item
                $.ajax({
                    url : "api/msg/addMessageCMS.php",
                    method : "POST",
                    dataType: "json",
                    data : {msgId:App.currentId,
                        guestMsgIdList: guestMsgIdlist,
                        startDate:$("#startDateInput").val() + " 00:00:00",
                        endDate:$("#endDateInput").val()+ " 00:00:00",
                        priority:$('#priority_checkbox').prop('checked')?1:0,
                        boardcast:boardcastVar,
                        status:"unread",
                        type:3,
                        source:"CMS",

                        title_en: $('#detailTitle_en').val(),
                        title_zh_hk: self.titleLangModel.zh_hk,
                        title_zh_cn: self.titleLangModel.zh_cn,
                        title_jp: self.titleLangModel.jp,
                        title_fr: self.titleLangModel.fr,
                        title_ar: self.titleLangModel.ar,
                        title_es: self.titleLangModel.es,
                        title_de: self.titleLangModel.de,
                        title_ko: self.titleLangModel.ko,
                        title_ru: self.titleLangModel.ru,
                        title_pt: self.titleLangModel.pt,

                        des_en: editor_en.getData(),
                        des_zh_hk: self.desLangModel.zh_hk,
                        des_zh_cn: self.desLangModel.zh_cn,
                        des_jp: self.desLangModel.jp,
                        des_fr: self.desLangModel.fr,
                        des_ar: self.desLangModel.ar,
                        des_es: self.desLangModel.es,
                        des_de: self.desLangModel.de,
                        des_ko: self.desLangModel.ko,
                        des_ru: self.desLangModel.ru,
                        des_pt: self.desLangModel.pt

                    }
                }).success(function(json){

                    if(json.status == 502){
                        alert(App.strings.sessionTimeOut);
                        location.reload();
                        return;
                    }

                    //alert("yeah");
                    console.log(json);
                    App.goUpperLevel();
                    //console.log("detailDes_en = " +  $('#detailDes_en').val());
                    //self.yesFunc();
                    //self.destroy();
                    //console.log("img array = " + that.imageObjArray);
                    //console.log(that.imageObjArray[that.preferImageIndex]);



                }).error(function(d){
                    console.log('error');
                    console.log(d);
                });
            }
        });

        if(App.currentItem.source != "CMS" && self.type != "new"){
            console.log("block!");
            $('#delete_btn').off();
            $('#save_btn').off();
        }

        //handle language select box
        $("#lang_selectionBox").change(function() {

            var editor_lang = CKEDITOR.instances.editor2;

            if($("#lang_selectionBox").val() != ""){
                self.currentLang = $("#lang_selectionBox").val();
            }

            //setup enable/disable the other language input box
            if($("#lang_selectionBox").val() == 'en'){
                //alert("disable");
                $('#container_lang input').attr('disabled','disabled');
                editor_lang.setReadOnly(true);
            }
            else{
                //alert("enable");
                $('#container_lang input').removeAttr('disabled');
                editor_lang.setReadOnly(false);
            }

            $('#container_lang input').val(eval("self.titleLangModel."+$("#lang_selectionBox").val()));

            //var string = eval("self.desLangModel."+$("#lang_selectionBox").val()).replace(/\n/g, "<br />");

            var string  = "";
            //alert(self.desLangModel.en.indexOf("<br />"));
            if(self.desLangModel.en.indexOf("<br />")== -1 && App.currentItem.lastUpdateBy == "TC3"){
                string =  eval("self.desLangModel."+$("#lang_selectionBox").val()).replace(/\n/g, "<br />");
                //string  = string.replace(/\r/g, "<br />");
            }
            else{
                string = eval("self.desLangModel."+$("#lang_selectionBox").val());
            }
            //string = string.replace(/\r/g, "<br />");

            editor_lang.setData(string);
        });

        $('#container_lang input').blur(function() {
            var editor_lang = CKEDITOR.instances.editor2;
            //alert("hi");
            eval("self.titleLangModel."+$("#lang_selectionBox").val()+ "= $('#container_lang input').val()");

            console.log(self.titleLangModel);
        });

        $("#addImageBtn").on('click',function(){
            App.addPhotoPopup = new App.AddPhotoPopup(
                {
                    root:self
                }
            );
        });

        $('#print_preview_btn').on('click',function() {
            App.previewPopupTabPrint = new App.PreviewPopupTabPrint(
                {
                    root:self,
                    previewType: "message",
                    itemTitle:$('#detailTitle_en').val(),
                    times:$("#startDateInput").val(),
                    description: editor_en.getData()
                }
            );
        });


    },

    postHandleGuestModel: function(_self){
        var self = _self;
        var guestLength  = self.guestModel.length;

        console.log("guest length = " + self.guestModel.length);

        for(var x = 0; x < guestLength; x++){
            console.log("guestModel = " + self.guestModel[x]);

            if (self.guestModel[x]!= null && self.guestModel[x].haveThisMsg == 1){

                self.selectGuestModel.push(self.guestModel[x]);

                self.guestModel.splice(x,1);

                x--;
            }
        }

        var select_guestLength  = self.selectGuestModel.length;
        console.log("self.guestModel = "+self.selectGuestModel );
        console.log("legthe = "+select_guestLength );
    },

    initGuestList: function(_self){
        console.log("initGuestList fire");
        var self = _self;
        var guestLength  = self.guestModel.length;
        console.log("self.guestModel = "+self.guestModel );
        console.log("self.select_guestModel = "+self.selectGuestModel );
        console.log("legthe = "+guestLength );

        //clear up first
        $("#guest_fixList").empty();

        for(var x = 0; x < guestLength; x++){

            var itemRoot =document.createElement('div');
            $(itemRoot).addClass("guest_item_container");

            $("#guest_fixList").append($(itemRoot));

            $(itemRoot).append("<div class='first_column'>"+ self.guestModel[x].room +"</div>");

            $(itemRoot).append("<div class='second_column'>" +self.guestModel[x].guestname +"</div>");
            $(itemRoot).append("<div class='add_button'" + "order ="+ x +" id="+"add"+x+">+</div>");

            $("#add"+x).on('click',function(){
                var index = $(this).attr("order");


                self.selectGuestModel.push(self.guestModel[index]);

                self.guestModel.splice(index,1);

                self.initGuestList(self);
                self.initSelectGuestList(self);
            });
        }
    },
    initSelectGuestList: function(_self){
        console.log("initSelectGuestList fire");
        var self = _self;
        var select_guestLength  = self.selectGuestModel.length;
        console.log("self.guestModel = "+self.selectGuestModel );
        console.log("legthe = "+select_guestLength );

        //clear up first
        $("#select_guest_fixList").empty();

        for(var x = 0; x < select_guestLength; x++){

            var itemRoot =document.createElement('div');
            $(itemRoot).addClass("guest_item_container");

            $("#select_guest_fixList").append($(itemRoot));

            $(itemRoot).append("<div class='first_column'>"+ self.selectGuestModel[x].room +"</div>");

            $(itemRoot).append("<div class='second_column'>" + self.selectGuestModel[x].guestname +"</div>");
            $(itemRoot).append("<div class='add_button pink'" + "order ="+ x +" id="+"delete"+x+">X</div>");

            $("#delete"+x).on('click',function(){
                var index = $(this).attr("order");

                self.guestModel.push(self.selectGuestModel[index]);

                self.selectGuestModel.splice(index,1);

                self.initGuestList(self);
                self.initSelectGuestList(self);
            });
        }
    },

    saveDescriptionLang: function(){
        var editor_lang = CKEDITOR.instances.editor2;
        eval("this.desLangModel."+$("#lang_selectionBox").val()+ "= editor_lang.getData()");
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW

        this.selectGuestModel = null;
        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});