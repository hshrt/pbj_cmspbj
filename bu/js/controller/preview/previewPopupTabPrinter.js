App.PreviewPopupTabPrint = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    itemTitle: "",
    parentTitle: "",
    itemImageLink: "",
    description: "",
    showTypeBox:true,
    parentId: 0,
    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        if(options && options.previewType){
            this.previewType = options.previewType;
        }
        if(options && options.itemTitle){
            this.itemTitle = options.itemTitle;
        }
        if(options && options.times){
            this.times = options.times;
        }
        if(options && options.itemImageLink){
            this.itemImageLink = options.itemImageLink;
        }
        if(options && options.description){
            this.description = options.description;
        }
        if(options && options.price){
            this.price = options.price;
        }

        $('body').css({'overflow':'hidden'});
        $(document).bind('scroll',function () {
            //window.scrollTo(0,0);
        });

        this.render();
    },
    events: {

        'click #cancel_btn'  : 'destroy'
    },
    setupUIHandler : function(){

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        $.ajax({
         url : "php/html/preview/previewPopupTabPrint.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);

                    $("#closeBtn").on('click',function(){
                        console.log("closeBtn clicked");
                        self.destroy();
                    });

                    if(self.previewType == "CMS_item") {
                        $("#title").html(self.itemTitle);
                        $("#item_pic").attr("src", self.itemImageLink);
                        $("#des").html(self.description);
                        $("#time").hide();
                    }
                    else if(self.previewType == "message") {
                        $("#title").html(self.itemTitle);
                        $("#item_pic").hide();
                        $("#time").html(self.times);
                        $("#des").html(self.description);
                    }



                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },

    clickConfirm : function(){

    },

    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        $(".popup_box_container").remove();
        this.undelegateEvents();

        $(document).unbind('scroll');
        $('body').css({'overflow':'visible'});


    },
    isHide : false
});