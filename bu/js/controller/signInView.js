App.SignInView = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',

    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        this.self = this;
        if(options && options.isEdit){
            this.isEdit = options.isEdit;
        }

        this.render();
    },
    events: {

    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        console.log("signInView render");
        var self = this;
        console.log("render SignInView");
        $.ajax({
         url : "php/html/signInView.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){
                    //$('.popup_box_container').show(true);

                    $("#signInBtn").on('click',function(){
                        $.ajax({
                            url : "api/login.php",
                            method : "POST",
                            dataType: "json",
                            data : {email:$("#email").val(), pwd:$("#pwd").val()}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                        }).success(function(json){
                            console.log(json);

                            if(json.status == 0){
                                //alert("login fail");
                                $("#warning").text(json.msg);
                            }
                            else if(json.status == 1){

                                if(App.serverType == "staging") {
                                    window.location.href = '#/property/' + App.hotelLocation + '/content/library';
                                    location.reload();
                                }
                                else if(App.serverType == "production") {
                                    var r = confirm("This is a production server, are you sure you want to proceed?");

                                    if (r == true) {
                                        window.location.href = '#/property/' + App.hotelLocation + '/content/library';
                                        location.reload();
                                    }
                                }

                            }

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    });

                    $('#pwd').bind("enterKey",function(e){
                        //do stuff here
                        $("#signInBtn").trigger("click");
                    });
                    $('#pwd').keyup(function(e){
                        if(e.keyCode == 13)
                        {
                            $(this).trigger("enterKey");
                        }
                    });

                    $('.tips').text("*This is " + App.serverType + " server");



                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });


    },

    check_scroll: function(e)
    {

        console.log("check_scroll");

        var elem = $(e.currentTarget);
        //console.log(elem[0].scrollHeight - elem.scrollTop());
        //console.log(elem.outerHeight());
        if (elem[0].scrollHeight - elem.scrollTop() - elem.outerHeight() < 5)
        {
            console.log("bottom");

            App.addPhotoPopup.currnentPage++;
            App.addPhotoPopup.loadPhoto( App.addPhotoPopup.currnentPage);
        }
    },

    close :function(){
        console.log("close fire");
    },
    getSelf: function(){
        return this.self;
    },
    destroy: function() {
        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        App.imageData = null;
        App.imageFileName = null;
        $(".popup_box_container").remove();
        this.undelegateEvents();

        $(document).unbind('scroll');
        $('body').css({'overflow':'visible'});

    },
    isHide : false
});