<div class="box detail_container" id=''>
    <div class="box-header" style="padding-top:0px">
        <div style='width:100%;height:auto;float:left'>
            <h2 id='itemName'></h2>
            <select id='lang_selectionBox' name='language'>

                <option value='en'>English</option>
                <option value='zh_cn'>Simplied Chinese</option>
                <option value='fr'>French</option>
                <option value='jp'>Japanese</option>
                <option value='ar'>Arabic</option>
                <option value='es'>Spanish</option>
                <option value='de'>German</option>
                <option value='ko'>Korean</option>
                <option value='ru'>Russian</option>
                <option value='pt'>Portuguese</option>
                <option value='zh_hk'>Traditional Chinese</option>
                <option value='tr'>Turkish</option>

            </select>
        </div>
    </div>
    <div class="box-body">
        <div class='half_width_container form-group' id = 'container_en'>
            <label for='title' id='titleLabel_en'>Title</label>
            <input id='detailTitle_en' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
            </div>
        <div class='half_width_container form-group' id = 'container_lang'>
            <label for='title' id='titleLabel_lang'>Title</label>
            <input id='detailTitle_lang' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus='' disabled>
            </div>


        <div class='half_width_container form-group' id = 'textEditor_en'>
            <label id = 'des_en'>
                Description
            </label>
            <div id="textareaContainer">
                <textarea cols="80" id="editor1" name="editor1" rows="10">
                </textarea>
            </div>
        </div>
        <div class='half_width_container form-group' id = 'textEditor_lang'>
            <label  id = 'des_lang'>
                Description
            </label>
            <div id="textareaContainer">
            <textarea cols="80" id="editor2" name="editor2" rows="10">

            </textarea>
             </div>
        </div>

        <div class='half_width_container form-group' id = 'priceSection'>
            <div>
                Price
            </div>
            <input id='price' class='form-control' type='text' name='price' tabindex='1'  min="0" max="10000" value='' maxlength="6"
                   autofocus=''>
        </div>

        <div class='half_width_container form-group' id = 'commandSection'>
            <div id="commandText">
                Command
            </div>
            <input id='command' class='form-control' type='text' name='command' tabindex='1' value='' maxlength="50"
                   autofocus=''>
        </div>

        <div class='half_width_container form-group' id = 'shortDesSection'>
            <div>
                Short Description
            </div>
            <input id='shortDes' class='form-control' type='text' name='command' tabindex='1' value='' maxlength="50"
                   autofocus=''>
        </div>
        <div class='half_width_container form-group' id = 'printSection'>
            <div>
               
            </div>
            <input id="printCheckBox" type="checkbox" />
            <label>print</label>
        </div>

        <div class="half_width_container form-group" id="container_start_time">
            <label>Start time</label>
            <input type="text" id="start_time" data-format="HH:mm" data-template="HH : mm" name="datetime">
        </div>

        <div class="half_width_container form-group" id="container_end_time">
            <label>End time</label>
            <input type="text" id="end_time" data-format="HH:mm" data-template="HH : mm" name="datetime">
        </div>


        <div class='sub_section'>Media</div>
        <!--<div class='dash'></div>-->
        <div id='ImageContainer'><div id='addImageBtn'></div></div>

        <div class='sub_section' id="iconSection">Icon</div>
        <!--<div class='dash'></div>-->
        <div id='IconContainer'><div id='addIconBtn'></div></div>

        <div class='half_width_container form-group' id = 'container_timeAvail'>
            <label for='title' id='timeAvail_en'>Available Time</label>
            <input id='timeAvail_en' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
        </div>

        <div class="half_width_container form-group" id="container_min">
            <label>Minimum of choice(s)</label>
            <select class="form-control" id="min_selector">

            </select>
        </div>
        <div class="half_width_container form-group" id="container_max">
            <label>Maximum of choice(s)</label>
            <select class="form-control" id="max_selector">

            </select>
        </div>

        <div class="half_width_container form-group" id="container_max_quantity">
            <label>Maximum quantity</label>
            <select class="form-control" id="max_quantity_selector">

            </select>
        </div>
        <div style="width:100%;float:left" id="optionsContainer">

            <div class='sub_section form-group' id="optionSetsText">Option Sets</div>
            <div class='half_width_container'>
                All option sets
            </div>
            <div class='half_width_container'>
                Selected option sets
            </div>
            <div class='half_width_container list form-group' id="option_list">
                <div class = 'fixList' id="option_fixList">
                </div>

            </div>

            <div class='half_width_container list form-group' id="selected_option_guest_list">
                <div class = 'fixList' id="selected_option_fixList">
                </div>

            </div>
            </div>




        <div class='save_cancel_container'>
            <div class='round_btn btn bg-maroon' id='delete_btn'>Delete Section</div>
            <div class='round_btn btn bg-olive' id='save_btn'>Save Section</div>
            <div class='round_btn btn bg-navy' id='preview_btn'>Tablet Preview</div>
            <div class='round_btn btn bg-navy' id='print_preview_btn'>Print Preview</div>
            </div>
        </div>
    </div>
</div>

