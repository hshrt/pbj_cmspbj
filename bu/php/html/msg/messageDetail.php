<div class='box detail_container'>
    <div class="box-header" style="padding-top:0px">
        <div style='width:100%;height:auto;float:left'>
            <h1 id='itemName'></h1>
            <select id='lang_selectionBox' name='language'>

                <option value='en'>English</option>
                <option value='zh_cn'>Simplied Chinese</option>
                <option value='fr'>French</option>
                <option value='jp'>Japanese</option>
                <option value='ar'>Arabic</option>
                <option value='es'>Spanish</option>
                <option value='de'>German</option>
                <option value='ko'>Korean</option>
                <option value='ru'>Russian</option>
                <option value='pt'>Portuguese</option>
                <option value='zh_hk'>Traditional Chinese</option>

            </select>
        </div>
    </div>

    <div class="box-body">
        <div class='half_width_container form-group' id = 'container_en'>
            <label for='title' id='titleLabel_en'>Title</label>
            <input id='detailTitle_en' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
            </div>
        <div class='half_width_container form-group' id = 'container_lang'>
            <label for='title' id='titleLabel_lang'>Title</label>
            <input id='detailTitle_lang' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus='' disabled>
            </div>


        <div class='half_width_container form-group' id = 'textEditor_en'>
            <label id = 'des_en'>
                Description
            </label>
            <div id="textareaContainer">
                <textarea cols="80" id="editor1" name="editor1" rows="10">
                </textarea>
            </div>
        </div>
        <div class='half_width_container form-group' id = 'textEditor_lang'>
            <label  id = 'des_lang'>
                Description
            </label>
            <div id="textareaContainer">
            <textarea cols="80" id="editor2" name="editor2" rows="10">
            </textarea>
             </div>
        </div>

        <div class='half_width_container form-group' id= 'container_checkIn'>
            <div class='half_width_container_small' id= 'container_checkInDate'>
                <label for='title' id='checkInDateLabel'>Start Date</label>
                <input id='startDateInput' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
            </div>
            <div class='half_width_container_small half_width_container_small_right' id= 'container_checkOutDate'>
                <label for='title' id='checkOutDateLabel'>End Date</label>
                <input id='endDateInput' class='form-control' type='text' name='title' tabindex='1' data-type='text' value='' autofocus=''>
            </div>
        </div>

        <div class='half_width_container form-group' id= 'container_checkOut'>

           <!-- <label for='title' id='Priority_label'>High Priority</label>
            <input id='priority_checkbox' type='checkbox' name='priority_checkbox'>-->

        </div>

        <div class='sub_section form-group'>Recipients</div>

        <div class='half_width_container form-group' style='width:100%' id= 'container_checkOut'>
            <div>
                <div style='width:100px;float:left'>
                    <input id='all_guest_radio' type='radio' name='all_guest_radio'>
                    <label for='title' id='all_guest_label'>All Guests</label>
                </div>
                <div style='width:155px;float:left'>
                    <input id='all_guest_popup_radio' type='radio' name='all_guest_radio'>
                    <label for='title' id='all_guest_popup_label'>All Guests (pop up)</label>
                </div>
                <div style='width:auto'>
                    <input id='custom_radio' type='radio' name='custom_radio'>
                    <label for='title' id='custom_label'>Custom</label>
                </div>
            </div>

        </div>
        <div  style=" width:100%; float:left" id="custom_guest_container">
            <div  style=" width:100%; float:left ">
                <div class='half_width_container form-group' >

                    <!--<input id="search" class='form-control' type='text' name='title' tabindex='1' data-type='text' placeholder="Search..." autofocus=''>-->
                </div>
            </div>

            <div class='half_width_container'>
                <label class='first_column' style="float:left" >Room</label>
                <label class='second_column' style="float:left" >Name</label>

            </div>

            <div class='half_width_container'>
                <label class='first_column' style="float:left" >Room</label>
                <label class='second_column' style="float:left" >Name</label>

            </div>

            <div class='half_width_container list form-group' id="guest_list">
                <div class = 'fixList' id="guest_fixList">
                </div>

            </div>

            <div class='half_width_container list form-group' id="select_guest_list">
                <div class = 'fixList' id="select_guest_fixList">
                </div>

            </div>
        </div>



        <div class='save_cancel_container'>
            <div class='round_btn btn bg-maroon' id='delete_btn'>Delete Section</div>
            <div class='round_btn btn bg-olive' id='save_btn'>Save Section</div>
            <div class='round_btn btn bg-navy' id='print_preview_btn'>Preview Message</div>
            </div>
        </div>
    </div>

</div>