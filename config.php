<?php
ini_set( "display_errors", true );
date_default_timezone_set( "Asia/Hong_Kong" );  // http://www.php.net/manual/en/timezones.php

//CMS server login info
define("DB_NAME","BSPPBJ");
define( "DB_DSN", "mysql:host=localhost;dbname=".DB_NAME.";charset=utf8" );
define( "DB_USERNAME", "root" );
define( "DB_PASSWORD", "ilikepbj");

//BPC server login info
define( "DB_DSN_BPC", "mysql:host=172.30.14.198;dbname=bpc;charset=utf8" );
define( "DB_USERNAME_BPC", "marco" );
define( "DB_PASSWORD_BPC", "marco");

//Weather server login info
define( "DB_DSN_WEATHER", "mysql:host=phkhgc.hsh-grt.com;dbname=Weather;charset=utf8" );
define( "DB_USERNAME_WEATHER", "marco" );
define( "DB_PASSWORD_WEATHER", "marco");

//Video Injection server URL
define("VIDEO_INJECTION_URL", '192.168.5.162:8000/');
define("VIDEO_INJECTION_USER", 'app');
define("VIDEO_INJECTION_PASSWORD", '1234');


//the base folder which contain all CMS source code
define("BASE_FOLDER", "cmspbj");
//the name of the location of the hotel shown in the URL
define("HOTEL_LOCATION", "beijing");
//this is either "production" or "staging"
define("SERVER_TYPE", "production");
//the header text shown in the browser
define("HEADER_TEXT", "HSH PBJ CMS");
//the version number
define("VERSION_NUM", "1.2.5");

//only useful for data Migration API
define( "ROOT_PATH", "http://172.30.14.197/cmspbj/" );


define("OK",200);
define("Created",201);
define("No_Content",204);
define("Not_Modified",304);
define("Bad_Request",400);
define("Unauthorized",401);
define("Forbidden",403);
define("Not_Found",404);
define("Method_Not_Allowed",405);
define("Gone",410);
define("Unsupported_Media_Type",415);
define("Unprocessable_Entity",422);
define("Too_Many_Requests",429);

define("Invalid_input",501);
define("Session_timeout",502);

function handleException( $exception ) {
  echo "Sorry, a problem occurred. Please try later.";
  error_log( $exception->getMessage() );
}

set_exception_handler( 'handleException' );
?>
