App.AddPhotoPopup = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#container',
    self:null,
    showPhotoAlbum:true,
    imageId:"",
    itemId:"",
    root:"",
    type:"normal",
    currnentPage:0,
    loading:false,
    imageObjArray : [],
    onlyShowUpLoad:false, //mean only show uploaded photo for user to choose
    uploadImageSizeLimit:5,
    height1:0,//these three store the height of the 3 column container
    height2:0,
    height3:0,

    cropWidth:1024,
    cropHeight:256,

    // It's the first function called when this view it's instantiated.
    initialize: function(options){
        this.self = this;
        if(options && options.showPhotoAlbum){
            this.showPhotoAlbum = options.showPhotoAlbum;
        }
        if(options && options.itemId){
            this.itemId = options.itemId;
        }

        if(options && options.root){
            this.root = options.root;
        }

        if(options && options.type){
            this.type = options.type;

            if(this.type == "icon"){
                this.cropWidth = 100;
                this.cropHeight = 100;
            }
        }

        if(options && options.onlyShowUpLoad){
            this.onlyShowUpLoad = options.onlyShowUpLoad;
        }


        $('body').css({'overflow':'hidden'});
        $(document).bind('scroll',function () {
            //window.scrollTo(0,0);
        });

        this.render();
    },
    events: {

        'click #cancel_btn'  : 'destroy'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        //alert($(window).width());

        var self = this;
        console.log("render AddPhotoPopup");
        $.ajax({
         url : "php/html/addPhotoPopup.php",
         method : "POST",
         dataType: "html",
         data : {}//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
         }).success(function(html){
             console.log(html);
            $('#container').append(html).
                promise()
                .done(function(){

                    $('.popup_box_container').show(true);

                    $("#closeBtn").on('click',function(){
                        self.destroy();
                    });

                    $('#uploadUseBtn').on('click',function(){
                        self.upload();
                    });

                    $("#takePictureField").on("change",self.gotPic);

                    $("#uploadMediaBtn").on("click",function(){

                        if(self.onlyShowUpLoad){
                            return;
                        }

                        if(App.imageData) {
                            $("#imageContainer,#tips").show();
                        }
                        $("#takePictureField").show();
                        $("#uploadImage").show();
                        $("#uploadUseBtn").text('Upload');

                        $("#uploadMediaBtn").addClass("focus");
                        $("#chooseExistBtn").removeClass("focus");

                        $("#photoStreamContainer").hide();

                        $("#stream1").empty();
                        $("#stream2").empty();
                        $("#stream3").empty();
                        self.currentPage = 0;

                        App.addPhotoPopup.currnentPage = 0;

                        $("#middle").css({"overflow":"visible"});

                        $("#middle").unbind('scroll');

                        $("#foot").show();
                    });

                    $("#chooseExistBtn").on("click",function(){
                        $("#imageContainer,#tips").hide();

                        $("#takePictureField").hide();
                        $("#uploadImage").hide();
                        $("#uploadUseBtn").text('Use');

                        $("#uploadMediaBtn").removeClass("focus");
                        $("#chooseExistBtn").addClass("focus");

                        $("#photoStreamContainer").show();

                        self.loadPhoto(self.currnentPage);

                        console.log("curerntPage =" + self.currentPage);

                        $("#middle").css({"overflow-y":"scroll"});
                        $("#middle").css({"overflow-x":"hidden"});

                        $("#middle").bind('scroll',self.check_scroll);

                        $("#foot").hide();
                    });

                    $("#general").on("click",function(){
                        App.addPhotoPopup.cropWidth = 1024;
                        App.addPhotoPopup.cropHeight = 768;
                        App.addPhotoPopup.setCropper();
                    });

                    $("#dining").on("click",function(){
                        App.addPhotoPopup.cropWidth = 1024;
                        App.addPhotoPopup.cropHeight = 256;

                        App.addPhotoPopup.setCropper();
                    });

                    $("#spa_rest").on("click",function(){
                        App.addPhotoPopup.cropWidth = 1280;
                        App.addPhotoPopup.cropHeight = 720;
                        App.addPhotoPopup.setCropper();
                    });

                    $("#icon").on("click",function(){
                        App.addPhotoPopup.cropWidth = 100;
                        App.addPhotoPopup.cropHeight = 100;
                        App.addPhotoPopup.setCropper();
                    });

                    $("#general").hide();
                    $("#dining").hide();
                    $("#spa_rest").hide();
                    $("#icon").hide();

                    if(self.onlyShowUpLoad){
                        $("#chooseExistBtn").hide();
                    }
                });

         }).error(function(d){
            console.log('error');
            console.log(d);
         });
    },
    setCropper: function(){

        if(App.imageData) {
            $('#imageContainer > img').cropper("destroy");
        }

        $('#imageContainer > img').cropper({
            aspectRatio:  App.addPhotoPopup.cropWidth /  App.addPhotoPopup.cropHeight,
            autoCropArea:0.8,
            guides: true,
            highlight: false,
            dragCrop: false,
            movable: false,
            resizable: false
        });
    },
    gotPic: function(event) {

        var self = this;

        $("#general").show();
        $("#dining").show();
        $("#spa_rest").show();
        $("#icon").show();

        if(event.target.files.length == 1 && event.target.files[0].type.indexOf("image/") == 0) {


            //get only filename without extension
            var file = event.target.files[0];
            var tempFileComponents = file.type.split("/");
            var fileExtension = tempFileComponents[1];
            //alert("file extension = " + fileExtension);

            App.imageFileExtension = fileExtension;
            App.imageFileName = file.name.substring(0,file.name.length - fileExtension.length-1);
            App.imageFileName = App.imageFileName.replace(/\s+/g, '');

            console.log('File size =' + file.size);
            console.log('max size =' + App.addPhotoPopup.self.uploadImageSizeLimit*1024*1024);
            //image file exceed upper limited size, not allow to pass
            if(file.size >  App.addPhotoPopup.self.uploadImageSizeLimit*1024*1024){
                $("#takePictureField").val('');
                alert("File size is too big. Please upload image less than "+ App.addPhotoPopup.self.uploadImageSizeLimit+"mb.");
                return;
            }
            else{
                //allow to remove previous cropped component.
                if(App.imageData) {
                    $('#imageContainer > img').cropper("destroy");
                }
            }

            var url = window.URL ? window.URL : window.webkitURL;

            $('#uploadImage').attr('src',url.createObjectURL(event.target.files[0]));

            var canvasImage = document.createElement("img");
            canvasImage.src = url.createObjectURL(event.target.files[0]);

            $("#imageContainer").show();
            $("#tips").show();

            canvasImage.onload = function(){

                console.log("Imaage onLoad fire");
                //change image data to base64
                var canvas = document.createElement('CANVAS'),
                    ctx = canvas.getContext('2d'),
                    outputFormat = "image/jpeg";

                canvas.height = canvasImage.height;
                canvas.width = canvasImage.width;
                ctx.drawImage(canvasImage, 0, 0);
                App.imageData = canvas.toDataURL(outputFormat);
                //alert("App.imageData = " + App.imageData);
                App.imageData = App.imageData.replace(/^data:image\/(png|jpg|jpeg);base64,/, "");

                //console.log("App.imageData = " + App.imageData);
                canvas = null;



                App.addPhotoPopup.setCropper();
            };


        }
        else{
            alert("Please select an image file.");
            $('#uploadImage').val("");
        }
    },

    upload: function(){
        //var self = this;
        App.showLoading();

       // App.imageData = $('#imageContainer > img').cropper("getDataURL").replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        console.log("App.addPhotoPopup.cropWidth = " + App.addPhotoPopup.cropWidth);

        //console.log(App.test.toDataURL());

        App.imageData = $('#imageContainer > img').cropper("getDataURL", { width:App.addPhotoPopup.cropWidth , height: App.addPhotoPopup.cropHeight }).replace(/^data:image\/(png|jpg|jpeg);base64,/, "");
        var self = this;
        console.log(self);
        console.log("App.imageData = " + App.imageData.length);

        if(!App.imageData || ! App.imageFileName){
            App.hideLoading();
            alert("Please select a photo first.");
            return;
        }

        $.ajax({
            url : "api/uploadPhoto.php",
            method : "POST",
            dataType: "json",
            data : {image:App.imageData, fileName:App.imageFileName,fileExtension: App.imageFileExtension, type: self.type}
        }).success(function(json){
            console.log(json);

            if(json.status == 502){
                alert(App.strings.sessionTimeOut);
                location.reload();
                return;
            }

            App.hideLoading();

            self.imageId = json.data.photoId;

            if(self.onlyShowUpLoad){//for add photo in Photo Section
                self.destroy();

                self.root.goBackToFirstPage();
            }
            else{ //for add photo in item's media Section
                $.ajax({
                    url : "api/addPhotoForItem.php",
                    method : "POST",
                    dataType: "json",
                    data : {mediaId:self.imageId, itemId:App.currentId, type: self.type}
                }).success(function(json){
                    console.log(json);

                    if(json.status == 502){
                        alert(App.strings.sessionTimeOut);
                        location.reload();
                        return;
                    }

                    self.destroy();
                    self.root.getPhoto();

                }).error(function(d){
                    console.log('error');
                    console.log(d);

                    App.hideLoading();
                    alert("There is upload error, please try again.");
                    self.destroy();
                });
            }
            //alert(self.imageId);
            //self.destroy();

        }).error(function(d){
            console.log('error');
            console.log(d);

            App.hideLoading();
            alert("Upload photo error, please try again.");
            alert(d);
            self.destroy();
        });
    },
    loadPhoto : function(page){

        var self = App.addPhotoPopup.getSelf();

        console.log("self = " + self);
        console.log("loadPhoto page = " + page);

        $.ajax({
            url : "api/getPhoto.php",
            method : "POST",
            dataType: "json",
            data : {page:page}
        }).success(function(json){
            console.log(json);


            var imageArray = json.data;

            self.imageObjArray = self.imageObjArray.concat(imageArray);
            console.log(imageArray);
            console.log("imageObjArray length = " + self.imageObjArray.length);
            var jj = [];

            var divArray = new Array();

            divArray = ["stream1","stream2","stream3"];

            var getShortestDiv = function(imgHeight){
                console.log("getShortestDiv imgHeight = " + imgHeight);

                var heightArray = [self.height1,self.height2,self.height3];

                var shortest = 0;
                for(var x = 0;x<heightArray.length;x++){
                    if(heightArray[x] < heightArray[shortest]){
                        shortest = x;
                    }
                }
                //heightArray[shortest]+= imgHeight;

                if(shortest == 0){
                    self.height1+=imgHeight;
                }
                else if(shortest == 1){
                    self.height2+=imgHeight;
                }
                else if(shortest == 2){
                    self.height3+=imgHeight;
                }//

                console.log('heightArray[shortest] = ' + heightArray[shortest]);

                console.log("height 1 = " + self.height1);
                console.log("height 2 = " + self.height2);
                console.log("height 3 = " + self.height3);

                return shortest;
            };

            var container;
            //self.imageId = json.data.photoId;
            for(var x = 0;x<imageArray.length;x++){
                console.log('imageArray = '+ imageArray[x].image);
                console.log('imageArray width = '+ imageArray[x].width);
                console.log('imageArray height = '+ imageArray[x].height);
                console.log('imageArray type = '+ imageArray[x].fileExt);

                var extension = "jpg";
                if(imageArray[x].fileExt == 'p'){
                    extension = "png";
                }
                var image = new Image();
                image.order =  x;
                image.src = "upload/"+imageArray[x].image+"_s."+extension;
                $(image).addClass("image");

                container = $("#"+divArray[getShortestDiv(imageArray[x].height)]);

                container.append("<div id=" + "pop_image"+(x+page*20) + "></div>");

                $("#pop_image"+(x+page*20)).addClass("imageRoot");

                $("#pop_image"+(x+page*20)).attr("order",(x+page*20) );

                $("#pop_image"+(x+page*20)).css({"position":"relative","float":"left"});

                $("#pop_image"+(x+page*20)).append($(image));

                $("#pop_image"+(x+page*20)).on("click",function(){
                    console.log("clicked");
                    var order = $(this).attr("order");
                    $.ajax({
                        url : "api/addPhotoForItem.php",
                        method : "POST",
                        dataType: "json",
                        data : {mediaId:self.imageObjArray[order].id, itemId:App.currentId, type: self.type}
                    }).success(function(json){
                        console.log(json);

                        if(json.status == 502){
                            alert(App.strings.sessionTimeOut);
                            location.reload();
                            return;
                        }

                        self.destroy();
                        self.root.getPhoto();

                    }).error(function(d){
                        console.log('error');
                        console.log(d);
                        alert("Upload photo error, please try again.");
                        self.destroy();
                    });
                });

            };


        }).error(function(d){
            console.log('error');
            console.log(d);
        });
    },
    check_scroll: function(e)
    {

        console.log("check_scroll");

        var elem = $(e.currentTarget);
        //console.log(elem[0].scrollHeight - elem.scrollTop());
        //console.log(elem.outerHeight());
        if (elem[0].scrollHeight - elem.scrollTop() - elem.outerHeight() < 60)
        {
            console.log("bottom");
            //alert("diu");
            App.addPhotoPopup.currnentPage++;
            App.addPhotoPopup.loadPhoto( App.addPhotoPopup.currnentPage);
        }
    },

    close :function(){
        console.log("close fire");
    },
    getSelf: function(){
        return this.self;
    },
    destroy: function() {
        //COMPLETELY UNBIND THE VIEW
        //this.undelegateEvents();
        //this.$el.removeData().unbind();
        App.imageData = null;
        App.imageFileName = null;
        $(".popup_box_container").remove();
        this.undelegateEvents();

        $(document).unbind('scroll');
        $('body').css({'overflow':'visible'});

    },
    isHide : false
});