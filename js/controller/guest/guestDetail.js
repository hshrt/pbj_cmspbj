App.GuestDetail = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    jsonObj: null,
    title: "Guest",
    initialize: function(options){
        if(options && options.parentId){
            this.parentId = options.parentId;
        }
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        self = this;

        console.log("should set: " + App.currentItem.salutation +". "+ App.currentItem.lastName);


        setTimeout(function(){App.subHead.setPathText(-1,App.currentItem.firstName +". "+ App.currentItem.lastName);}, 1000);


        $.ajax({
            url : "php/html/guest/guestDetail.php",
            method : "POST",
            dataType: "html",
            data : { }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
        }).success(function(html){
            $(self.el).append(html).
                promise()
                .done(function(){
                    //self.loadPhoto(self.page);
                    console.log("halo");
                    $("#itemName").text(App.currentItem.salutation +". "+ App.currentItem.lastName);

                    //set date picker
                    $( "#checkInDateInput" ).datepicker({ dateFormat: 'yy-mm-dd' });
                    $( "#checkOutDateInput" ).datepicker({dateFormat: 'yy-mm-dd' });

                    $("#delete_btn").text("Delete Guest");
                    $("#save_btn").text("Save Guest");


                    console.log(App.currentItem.salutation);

                    $("#salutationInput").val(App.currentItem.salutation);
                    $("#firstNameInput").val(App.currentItem.firstName);
                    $("#lastNameInput").val(App.currentItem.lastName);
                    $("#roomNumInput").val(App.currentItem.room);
                    $("#checkInDateInput").val(App.currentItem.checkIn.substr(0,10));
                    $("#checkInTimeInput").val(App.currentItem.checkIn.substr(11,5));
                    $("#checkOutDateInput").val(App.currentItem.checkOut.substr(0,10));
                    $("#checkOutTimeInput").val(App.currentItem.checkOut.substr(11,5));

                    $("#reserveIdInput").val(App.currentItem.reserveId);
                    $("#memberIdInput").val(App.currentItem.memberId);

                    $("#delete_btn").on("click",function(){


                        App.yesNoPopup = new App.YesNoPopup(
                            {
                                yesFunc:function()
                                {
                                    $.ajax({
                                        url : "api/guest/deleteGuest.php",
                                        method : "POST",
                                        dataType: "json",
                                        data : {guestId:App.currentItem.id }//canParking: UserData.canParking, canInvite: UserData.canInvite , ticket : UserData.Ticket
                                    }).success(function(json){

                                        console.log(json);
                                        //self.yesFunc();
                                        App.yesNoPopup.destroy();
                                        if(json.status == 1){

                                            App.goUpperLevel();
                                        }
                                        else{
                                            alert("Delete Fail. Error occur");
                                        }

                                    }).error(function(d){
                                        console.log('error');
                                        console.log(d);
                                    });
                                },
                                msg:"Are you sure to delete this item?"
                            }
                        );
                    });

                    $("#save_btn").on("click",function(){


                        $.ajax({
                            url : "api/guest/updateGuest.php",
                            method : "POST",
                            dataType: "json",
                            data : {guestId:App.currentItem.id,
                                    salutation:$("#salutationInput").val(),
                                    firstName:$("#firstNameInput").val() ,
                                    lastName:$("#lastNameInput").val() ,
                                    room:$("#roomNumInput").val() ,
                                    checkIn:$("#checkInDateInput").val() + " " + $("#checkInTimeInput").val() ,
                                    checkOut:$("#checkOutDateInput").val() + " " + $("#checkOutTimeInput").val() ,
                                    reserveId:$("#reserveIdInput").val(),
                                    memberId:$("#memberIdInput").val()
                            }
                        }).success(function(json){

                            console.log(json);
                            //self.yesFunc();

                            if(json.status == 1){

                                App.goUpperLevel();
                            }
                            else{
                                alert("Update Fail. Error occur");
                            }

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    });

                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function(){
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library"?"Page":self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});