App.AviationList = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    jsonObjs: null,
    jsonObjs_filtered:null,
    type:null,
    title: "",
    initialize: function(options){
        if(options && options.type){
            this.title = this.type = options.type;
        }
        this.render();
    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){

        //Insert Table Header




        /*$(this.el).append(
        "<div id='itemListContainer'>"+
        "<div class='table-header'>"+
            "<h1>Guests</h1>"+
            "<div class='submit round_btn' id='addBtn'>+Add Guest</div>"+
            "<input id='search' type='text' name='title' tabindex='1' data-type='text' placeholder='Search...'' autofocus=''>"+
        "</div>"+
            "<table id='headerTable'>"+
            "<tr class='headRow2'>"+
            tableHeaderString+
            "</tr></table>"+

            "<table id='resultTable'>"+
            "<tr class='headRow'>"+
            tableHeaderString+
            "</tr></table>"+

        "</table></div>"

            );*/

        var self = this;

        $.ajax({
            url : "php/html/itemList.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){

            console.log(html);

            $(self.el).append(html).promise()
                .done(function() {
                    //alert("done");
                    self.postUISetup();
                    $("#boxer").after($("#itemListContainer"));
                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });

    },
    postUISetup:function(){
        var self = this;

        $("#item_list_head_title").text(upperFirstChar(self.title));

        $('#addBtn').on('click',function(){
            //alert("delete item id = " + App.currentId);
            self.showPopup();
        });

        //setup add btn text
        $('#addBtn').text("+Add " + upperFirstChar(self.title));

        App.showLoading();

        var _url = self.title == "airline"?"api/flightStat/getAllAirline.php":"api/flightStat/getAllAirport.php";

        $.ajax({
            url : _url,
            method : "GET",
            dataType: "json",
            data : {}
        }).success(function(json){
            //setTimeout(App.hideLoading, 1000);

            App.hideLoading();

            console.log(json);
            self.jsonObjs = json.data;

            self.drawList(self.jsonObjs);

            //self.drawList(self.jsonObjs);
            console.log("jsonObj = " + self.jsonObjs);
            console.log(App.idArray);

        }).error(function(d){
            App.hideLoading();
            console.log('error');
            console.log(d);
        });

        $('#search').on('keyup', function() {
            if (this.value.length > 0) {

                self.jsonObjs_filtered = new Array();
                self.jsonObjs_filtered = searchStringInArrByNameForAviation($("#search").val(),self.jsonObjs);

                if(self.jsonObjs_filtered.length == 0){
                    $("#resultTable").empty();

                    $("#noResultMsg").show();

                }
                else{
                    console.log("yeah");

                    $("#noResultMsg").hide();

                    self.filtering = true;

                    self.drawList(self.jsonObjs_filtered);
                }
            }
            else{
                $("#paginationContainer").show();
                $("#noResultMsg").hide();
                self.filtering = false;
                self.drawList(self.jsonObjs);
            }
        });
    },

    changeDate: function(str1){
        //var hour   = parseInt(str1.substring(6,10));    2014-11-25 12:00:00
    // str1 format should be dd/mm/yyyy. Separator can be anything e.g. / or -. It wont effect
        var year   = parseInt(str1.substr(0,4));
        var month  = parseInt(str1.substr(5,2));
        var day   = parseInt(str1.substr(8,2));
        var hour   = parseInt(str1.substr(11,2));
        var min   = parseInt(str1.substr(14,2));
        var sec  = parseInt(str1.substr(17,2));

        console.log("year = " + year);
        console.log("month = " + month);
        console.log("day = " + day);

        var date1 = new Date(year, month, day,hour,min,sec);
        return date1.getFullYear()+"-"+(date1.getMonth()+1) +"-"+date1.getDate() + " "+date1.getHours()+1<10?"0":""+date1.getHours()+1+":"+ date1.getMinutes()<10?"0":""+ date1.getMinutes();
    },
    drawList: function(objects){

        var self = this;

        var tableHeaderString = "<tr><th class='tableHead'>Name</th>"+
            "<th class='tableHead'>IATA code#</th></tr>";

        $("#resultTable").empty();

        $("#resultTable").append(tableHeaderString);

        for(var y = 0; y<objects.length;y++){

            $("#resultTable").append(
                "<tr id='"+objects[y].id+"' style='height:30px'><td>"+
                "<div class='titleText'>"+objects[y].name +"</div>"+"</td>"+
                    "<td style='width:50%'>"+objects[y].IATA+"</td>"+
                    "</tr>");

            $("#"+objects[y].id).attr( "index", y );

            (function (jsonitem) {
                //alert(Area);
                $("#"+jsonitem.id).on('click',function(){
                    console.log(jsonitem);
                    App.addAviationPopup = new App.AddAviationPopup(
                        {
                            title: self.title,
                            fxObj: jsonitem
                        }
                    );
                });
            })(objects[y]);
        }
    },
    showPopup: function(){
        var self = this;
        //alert(self.title);
        App.addAviationPopup = new App.AddAviationPopup(
            {

                title: self.title,
                aviObj: null
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        $("#item_list_boxer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});