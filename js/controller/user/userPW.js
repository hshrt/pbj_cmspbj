App.UserPW = Backbone.View.extend({

    // el - stands for element. Every view has a element associate in with HTML content will be rendered.
    el: '#content_container',
    // It's the first function called when this view it's instantiated.
    parentId: 0,
    jsonObj: null,
    title: "Guest",
    initialize: function(options){
        if(options && options.parentId){
            this.parentId = options.parentId;
        }
        if(options && options.listTitle){
            this.title = options.listTitle;
        }
        this.render();

    },
    events: {
        'click #closeBtn_start' : 'backToIndex'
    },
    // $el - it's a cached jQuery object (el), in which you can use jQuery functions to push content. Like the Hello World in this case.
    render: function(){
        self = this;

        //console.log("should set: " + App.currentItem.salutation +". "+ App.currentItem.lastName);


        //setTimeout(function(){App.subHead.setPathText(-1,App.currentItem.firstName +". "+ App.currentItem.lastName);}, 500);


        $.ajax({
            url : "php/html/user/userPW.php",
            method : "POST",
            dataType: "html",
            data : { }
        }).success(function(html){
            $(self.el).append(html).
                promise()
                .done(function(){
                    //self.loadPhoto(self.page);
                    console.log("halo");
                    $("#itemName").text(App.userEmail);



                    /*$("#delete_btn").text("Delete User");
                    $("#save_btn").text("Save User");*/


                    //console.log(App.currentItem.salutation);

                    $("#save_btn").on("click",function(){

                        if($("#oldPWInput").val() == "" || $("#newPW1Input").val() == "" || $("#newPW2Input").val() == ""){
                            alert("Please fill all fields.");
                            return;
                        }

                        if( $("#newPW1Input").val() != $("#newPW2Input").val()){
                            alert("Two New password doesn't match each other");
                            return;
                        }

                        console.log(md5($("#oldPWInput").val()));
                        $.ajax({
                            url : "api/user/changePW.php",
                            method : "POST",
                            dataType: "json",
                            data : {userId:App.userId,
                                    oldPassword:md5($("#oldPWInput").val()) ,
                                    newPassword:md5($("#newPW1Input").val())
                            }
                        }).success(function(json){

                            console.log(json);

                            if(json.status == 501){
                                alert(json.msg);
                                return;
                            }

                            if(json.status == 502){
                                alert(App.strings.sessionTimeOut);
                                location.reload();
                                return;
                            }

                            //self.yesFunc();

                            if(json.status == 1){
                                alert("Password changed.");
                                var url = window.location.protocol+'//'+window.location.hostname+'/'+App.baseFolder+'/#/';

                                url += "property/"+App.hotelLocation +"/content/library";
                                window.location = document.URL = url;
                            }
                            else{
                                alert("Update Fail. Error occur");
                            }

                        }).error(function(d){
                            console.log('error');
                            console.log(d);
                        });
                    });

                });

        }).error(function(d){
            console.log('error');
            console.log(d);
        });


    },
    showPopup: function(){
        var self = this;
        //alert(self.title);
        App.createItemPopup = new App.CreateItemPopup(
            {

                title: self.title == "Library"?"Page":self.title,
                parentId: self.parentId
            }
        );
    },
    backToIndex: function(){
        window.parent.PeriEvent.backToIndex();
    },
    moveToTheme: function(){
        //this.destroy();
        $(this.el).hide();
        $("#blackTemp").show();
        $("#content").show();
    },
    showUp: function(){
        $(this.el).show();
        this.isHide = false;
        $("#blackTemp").hide();
    },
    close :function(){
        console.log("close fire");
    },
    destroy: function() {

        //COMPLETELY UNBIND THE VIEW


        //this.$el.removeData().unbind();

        //Remove view from DOM
        $("#itemListContainer").remove();
        this.undelegateEvents();
        //this.initialize();
        //Backbone.View.prototype.remove.call(this);

    },
    isHide : false
});