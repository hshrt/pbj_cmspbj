<?php
	$folder = isset($_REQUEST['folder'])?$_REQUEST['folder']:'movie';
?>
<html>
	<head>
		<title>HSH PBJ Movie Test</title>
		<style>
			.imageThumb {
				cursor: pointer;
				transition: 0.3s;
			}

			.imageThumb:hover {
				opacity: 0.7;
			}

			.modal {
				display: none;
				position: fixed;
				z-index: 1;
				padding-top: 100px;
				left: 0;
				top: 0;
				width: 100%;
				height: 100%;
				overflow: auto;
				background-color: rgb(0,0,0);
				background-color: rgba(0,0,0,0.9);
			}

			.modal-content {
				margin: auto;
				display: block;
				max-width: 700px;
				max-height: 600px;
				min-width: 400px;
			}

			#caption {
				margin: auto;
				display: block;
				width: 80%;
				max-width: 700px;
				text-align: center;
				color: #ccc;
				padding: 10px 0;
				height: 150px;
			}

			.modal-content, #caption {
				animation-name: zoom;
				animation-duration: 0.6s;
			}

			@keyframes zoom {
				from {transform:scale(0)}
				to {transform:scale(1)}
			}

			.close {
				position: absolute;
				top: 15px;
				right: 35px;
				color: #f1f1f1;
				font-size: 40px;
				font-weight: bold;
				transition: 0.3s;
			}

			.close:hover,
			.close:focus {
				color: #bbb;
				text-decoration: none;
				cursor: pointer;
			}

			@media only screen and (max-width: 700px){
				.modal-content {
					width: 100%;
				}
			}
		</style>
	</head>
	<body>
		<div class="header">
			<h1>HSH PBJ Movie List</h1>
		</div>
<?php
			for ($x = 1; $x <= 50; $x++) {
				echo "\t\t<div><h3>Movie ", $x, "</h3>";
				echo "<img class='imageThumb' style='height:200px' src='", $folder, "/small", $x, ".png' />";
				echo "<img class='imageThumb' style='height:200px' src='", $folder, "/big", $x, "_c.jpg' />";
				echo "<img class='imageThumb' style='height:200px' src='", $folder, "/big", $x, "_e.jpg' />";
				echo "</div>\n";
			}
?>
		<div id="imageModal" class="modal">
			<span class="close">&times;</span>
			<img class="modal-content" id="bigImage" />
			<div id="caption"></div>
		</div>

		<script>
			var modal = document.getElementById("imageModal");

			var img = document.getElementsByClassName("imageThumb");
			var modalImg = document.getElementById("bigImage");
			var captionText = document.getElementById("caption");
			for (var i = 0; i < img.length; i++) {
				img[i].onclick = function(){
					modal.style.display = "block";
					modalImg.src = this.src;
					captionText.innerHTML = this.alt;
				}
			}

			var span = document.getElementsByClassName("close")[0];
			span.onclick = function() { 
				modal.style.display = "none";
			}
		</script>
	</body>
</html>
