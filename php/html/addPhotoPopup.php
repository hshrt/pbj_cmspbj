<!-- popup box setup -->
<div class='popup_box_container'>
    <div class='popup_box photo_box'>
        <div id="head">
            <div class='closeBtn' id='closeBtn'></div>
            <div class="menuContainer">
                <div class="menuItem focus" id="uploadMediaBtn">
                    Uploading Media
                </div>
                <div class="menuItem" id="chooseExistBtn">
                    Choosing Existing
                </div>
            </div>
        </div>

        <div id="middle">
            <div style='float:left; margin-right: 5px;' class='submit round_btn btn bg-olive' id="general">general</div>
            <div style='float:left; margin-right: 5px;' class='submit round_btn btn bg-olive' id="dining">in room dining</div>
            <div style='float:left; margin-right: 5px;' class='submit round_btn btn bg-olive' id="spa_rest">spa/restaurant</div>
            <div style='float:left; margin-right: 5px;' class='submit round_btn btn bg-olive' id="icon">icon</div>
            <!--upload media UI-->
            <input id="takePictureField" type="file" accept="image/*" >
            <div id="imageContainer">
            <img id="uploadImage"/>
            </div>
            <div id="tips">
                Drag to move the picture.</br> Scroll the mouse wheel to zoom in/out.
            </div>
            <div id="photoStreamContainer">
                <div id="stream1"></div>
                <div id="stream2"></div>
                <div id="stream3"></div>
            </div>
        </div>

        <div id="foot">
            <div class="actions">
                <div style='float:right' class='submit round_btn btn bg-olive' id="uploadUseBtn">Upload</div>
            </div>
        </div>
    </div>
</div>